function Listener() {
	this.__listeners = [];
}

Listener.prototype = {
	listen: function(callback) {
		this.__listeners.push(callback);
	},
	unlisten: function(callback) {
		/* probably not implemented cause we're not going to use it */
	},
	_destroyListener: function() {
		delete this.__listeners;
	},
	_broadcast: function() {
		for (var i = 0; i < this.__listeners.length; ++i) {
			var callback = this.__listeners[i];
			callback.apply(null, arguments);
		}
	},
}

function inherit(derived, base) {
	var basePrototype = Object.create(base.prototype);
	derived.prototype = Object.assign(basePrototype, derived.prototype);

	derived.base = base;
	derived.prototype.constructor = derived;
}

Object.defineProperty(Object.prototype, "callBaseMethod", 
	{
		value: function (derived, methodName, args) {
			var toCall;
			if (methodName) {
				var baseClass = derived.base;
				while (toCall === undefined) {
					toCall = baseClass.prototype[methodName];
					baseClass = baseClass.base;
					if (baseClass == undefined) {
						break;
					}
				}
			}
			else {
				toCall = derived.base;
			}
			if (toCall) {
				return toCall.apply(this, args);
			}
		},
		enumerable: false,
});

function isAncestor(node, supposedAncestor) {
	while (node) {
		if (node === supposedAncestor) {
			return true;
		}
		node = node.parentNode;
	}
}

function getWindow(node) {
	return getDocument(node).defaultView;
}

function getDocument(node) {
	return node.ownerDocument;
}

function getSVG(key) {
	var container = document.getElementById(key);
	if (container) {
		var clone = container.cloneNode(true);
		clone.id = "";
		return clone;
	}
}

function getRangeSelected(root) {
	var doc = root.ownerDocument;
	var win = doc.defaultView;
    if (win.getSelection) {
        var sel = win.getSelection();
		var node = sel.baseNode || sel.anchorNode;
		if (isAncestor(node, root)) {
			return sel.getRangeAt(0);
		}
	}
}

function wrapFontName(name) {
	if (name.charAt(0) === "\"") {
		return name;
	}
	return "\"" + name + "\"";
}

function setFirstClearIfDifferent(value, object, property) {
	if (object[property] === undefined) {
		object[property] = value;
	}
	else if (object[property] !== value) {
		object[property] = "";
	}
}

function unwrapFontName(name) {
	if (name.charAt(0) === "\"") {
		return name.substring(1,name.length-1);
	}
	return name;
}

function replaceNode(node, nodeToReplace) {
	node.parentNode.replaceChild(nodeToReplace, node);
}

function isElement(node) {
	return node.nodeType === Node.ELEMENT_NODE;
}

function scaleFontSize(fontSize, factor) {
	var numSize = Number.parseFloat(fontSize);
	if (numSize) {
		return Math.round(numSize * factor) 
	}
}

function leftmostLeaf(node) {
  while (node.childNodes.length > 0) {
    node = node.childNodes[0];
  }
  return node;
}

function rightmostLeaf(node) {
  while (node.childNodes.length > 0) {
    node = node.childNodes[node.childNodes.length - 1];
  }
  return node;
}

function nextLeaf(root, node) {
  for (;;) {
    if (node === root) return null;
    var siblings = node.parentNode.childNodes;
    for (var i = 0; i < siblings.length - 1; ++i) {
      if (siblings[i] === node) {
        return leftmostLeaf(siblings[i + 1]);
      }
    }
    node = node.parentNode;
  }
}

function addCssClass(node, className) {
	if (!hasCssClass(node, className)) {
		node.className = node.className + " " + className + " ";
	}
}

function removeCssClass(node, className) {
	node.className = node.className.replace(" " + className + " ", "");
}

function hasCssClass(node, className) {
	return node.className.search(" " + className + " ") >= 0;
}

function setCookie(key, value) {
	document.cookie = key + "=" + value + "; path=/";
}

function getCookie(key) {
	var find = key + "=";
	var cookies = document.cookie;
	var cookieValues = cookies.split(';');
	for(var i = 0; i < cookieValues.length; i++) {
		var keyValue = cookieValues[i];
		keyValue = keyValue.replace(/^\s+/,"");
		if (keyValue.indexOf(find) == 0) {
			var value = keyValue.substring(find.length, keyValue.length);
			return value;
		}
	}
	return null;
}

function getComputedBackgroundColor(root, elem) {
	if (!root) {
		root = getDocument(elem);
	}
	while (elem !== root) {
		var bg = window.getComputedStyle(elem).getPropertyValue("background-color");
		if (!isTransparent(bg)) {
			return bg;
		}
		elem = elem.parentNode;
	}
	return "rgb(255, 255, 255)";
}

function isBold(style) {
  var value = style.getPropertyValue("font-weight");
  return value === "bold" || value > 400;
}

function isItalic(style) {
  var value = style.getPropertyValue("font-style");
  return value === "italic";
}

function getFontFamily(style) {
	var value = style.getPropertyValue("font-family");
	return value;
}

function isTransparent(color) {
	if (color === "rgba(0, 0, 0, 0)") {
		return true;
	}
	if (color === "transparent") {
		return true;
	}
	return false;
}

// Underline is weird (see linter). If a node is underlined (after linting),
// its first child will be a nonsyntax span with text-decoration:underline.
function isUnderlined(node) {
  // For unlinted DOM, or a text node or nonsyntax node in linted DOM:
  var props = ['textDecoration', 'textDecorationLine'];
  var ancestor = node;
  while (ancestor) {
    if (ancestor.tagName && ancestor.tagName.toLowerCase() === "u") return true;
    if (ancestor.style) {
      for (var i = 0; i < props.length; ++i) {
        var prop = ancestor.style[props[i]];
        if (prop && prop.search("underline") >= 0) return true;
      }
    }
    ancestor = ancestor.parentNode;
  }
  // For linted DOM:
  return node.children.length > 0 &&
         hasCssClass(node.children[0], "nonsyntax") &&
         node.children[0].style.textDecoration === "underline";
}

// Returns the subset of child elements that are not marked as nonsyntax nodes.
function syntaxChildren(node) {
  var children = [];
  for (var i = 0; i < node.children.length; ++i) {
    if (!hasCssClass(node.children[i], "nonsyntax")) {
      children.push(node.children[i]);
    }
  }
  return children;
}

// If `node` is a "logical text node" (i.e., either a text node itself, or a
// nonsyntax node with a child text node), returns the real text node;
// otherwise returns null. This function assumes that the DOM has been linted.
function logicalTextNode(node) {
  if (!node) return null;
  if (node.nodeType === Node.TEXT_NODE) return node;
  if (node.nodeType === Node.ELEMENT_NODE && hasCssClass(node, "nonsyntax")) {
    return node.childNodes[0];
  }
  return null;
}
