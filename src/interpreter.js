// ============================================================================
// Call stack
// ============================================================================

// Keys in the symbol table (and arguments to Assign and fetch) are computed
// color strings of the form "rgb(x, x, x)". This is also what Color objects
// (from color.js) return from their toString method, so those can be used too.
function StackFrame() {
  this.args = [];
  this.symbolTable = {};
}

StackFrame.prototype.pushArg = function(value) {
  this.args.push(value.clone());
};

function Stack() {
  this.__frames = [new StackFrame()];
}

Stack.roygbivIndex = function(color) {
  var index = {
    "rgb(255, 0, 0)":     0,  // red
    "rgb(255, 165, 0)":   1,  // orange
    "rgb(255, 255, 0)":   2,  // yellow
    "rgb(0, 128, 0)":     3,  // green
    "rgb(0, 0, 255)":     4,  // blue
    "rgb(75, 0, 130)":    5,  // indigo
    "rgb(238, 130, 238)": 6,  // violet
  }[color];
  return index === undefined ? -1 : index;
}

Stack.prototype.fetchArg = function(index) {
  if (index < 0 || index >= this.__frames[0].args.length) return null;
  return this.__frames[0].args[index];
};

Stack.prototype.redshift = function() {
  return this.__frames[0].args.shift();
};

Stack.prototype.assign = function(color, value) {
  this.__frames[0].symbolTable[color] = value.clone();
};

Stack.prototype.fetch = function(color) {
  for (var i = 0; i < this.__frames.length; ++i) {
    var value = this.__frames[i].symbolTable[color];
    if (value) return value;
  }
  return null;
};

// We actually unshift instead of push, so that the top of the stack is at
// index 0.
Stack.prototype.pushFrame = function(frame) {
  this.__frames.unshift(frame);
};

Stack.prototype.popFrame = function() {
  this.__frames.shift();
};

// ============================================================================
// Interpreter
// ============================================================================

function Interpreter() {
  this.__stack;
  this.__nextId = 0;  // For assigning IDs to function definition nodes.
  this.__stdin = "";
}

Interpreter.__oops = function(node, error) {
  addCssClass(node, "oops");
  throw error;
};

// ============================================================================
// Built-ins: Control structures
// ============================================================================

Interpreter.prototype.__honeydew = function(node) {
  var value = Value.makeNumericValue(0);
  var children = syntaxChildren(node);
  for (var i = 0; i < children.length; ++i) {
    value = this.__interpretSubtree(children[i]);
  }
  return value;
};

Interpreter.prototype.__ifElse = function(node) {
  var children = syntaxChildren(node);
  if (children.length % 2 !== 1) {
    Interpreter.__oops(node,
        "Syntax error: #1FE15E requires an odd number of arguments");
  }
  for (var i = 0; i < children.length - 1; i += 2) {
    if (this.__interpretSubtree(children[i]).isTrue()) {
      return this.__interpretSubtree(children[i + 1]);
    }
  }
  return this.__interpretSubtree(children[children.length - 1]);
};

Interpreter.prototype.__teal = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 2) {
    Interpreter.__oops(node,
        "Syntax error: teal requires exactly two arguments");
  }
  var body;
  do {
    body = this.__interpretSubtree(children[0]);
  } while (!this.__interpretSubtree(children[1]).isTrue());
  return body;
};

Interpreter.prototype.__deepskyblue = function(node) {
  if (syntaxChildren(node).length > 0) {
    Interpreter.__oops(node,
        "Syntax error: deepskyblue requires zero arguments");
  }
  return this.__stack.redshift();
};

// Helper function for ghostwhite and fuchsia, which do not actually evaluate
// their argument.
Interpreter.prototype.__introspectChild = function(node, caller) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: " + caller + " requires exactly one argument");
  }
  if (syntaxChildren(children[0]).length > 0) {
    Interpreter.__oops(node,
        "Syntax error: " + caller + " argument cannot have child nodes");
  }
  if (children[0].style.fontStyle === "italic") {
    Interpreter.__oops(node,
        "Syntax error: " + caller +
        " argument cannot be a function definition");
  }
  if (children[0].style.backgroundColor !== "transparent") {
    Interpreter.__oops(node,
        "Syntax error: " + caller + " argument cannot have a background color");
  }
  var color = Color.parseString(children[0].style.color);
  if (isUnderlined(children[0])) {
    if (children[0].style.fontWeight === "bold") {
      Interpreter.__oops(node, "Syntax error: bold + underline");
    }
    return { type: "literal", color: color };
  }
  if (children[0].style.fontWeight === "bold") {
    var index = Stack.roygbivIndex(color);
    if (index >= 0) return { type: "funcarg", color: color, index: index };
    for (var i = 0; i < Color.builtinFunctions.length; ++i) {
      if (color.equals(Color.builtinFunctions[i])) {
        return { type: "builtinfunc", color: color };
      }
    }
    return { type: "undefbuiltin", color: color };
  }
  return { type: "user", color: color };
};

Interpreter.prototype.__ghostwhite = function(node) {
  var childDesc = this.__introspectChild(node, "ghostwhite");
  switch (childDesc.type) {
    case "literal":
      return Value.makeFalse();
    case "funcarg":
      return Value.makeBooleanValue(!this.__stack.fetchArg(childDesc.index));
    case "builtinfunc":
      return Value.makeFalse();
    case "undefbuiltin":
      return Value.makeTrue();
    case "user":
      return Value.makeBooleanValue(!this.__stack.fetch(color));
  }
  Interpreter.__oops(node,
      "Internal error: oops, ghostwhite doesn't handle all cases: " +
      childDesc.type);
};

Interpreter.prototype.__scalar = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: #5CA1A2 requires exactly one argument");
  }
  // TODO: Should fndefs return true?
  return Value.makeBooleanValue(
             this.__interpretSubtree(children[0]).type === "scalar");
};

Interpreter.prototype.__fuchsia = function(node) {
  var childDesc = this.__introspectChild(node, "fuchsia");
  switch (childDesc.type) {
    case "literal":
      Interpreter.__oops(node,
          "Syntax error: fuchsia argument cannot be a numeric literal");
    case "funcarg":
      var value = this.__stack.fetchArg(childDesc.index);
      if (!value) {
        Interpreter.__oops(node,
            "Undefined function argument: " + childDesc.color);
      }
      if (value.type === "fndef") {
        return value;
      } else {
        return Value.makeFalse();
      }
    case "builtinfunc":
      Interpreter.__oops(node,
          "Syntax error: fuchsia argument cannot be a built-in function");
    case "undefbuiltin":
      Interpreter.__oops(node,
          "Syntax error: undefined built-in " + childDesc.color);
    case "user":
      var value = this.__stack.fetch(childDesc.color);
      if (!value) {
        Interpreter.__oops(node, "Undefined variable: " + childDesc.color);
      };
      if (value.type === "fndef") {
        return value;
      } else {
        return Value.makeFalse();
      }
  }
  Interpreter.__oops(node,
      "Internal error: oops, fuchsia doesn't handle all cases: " +
      childDesc.type);
};

// ============================================================================
// Built-ins: Comparisons and Boolean operations
// ============================================================================

// Helper function used by __plum, __lesser, and __bigger.
Interpreter.prototype.__comparison = function(node, expected_sign) {
  var children = syntaxChildren(node);
  var prev;
  for (var i = 0; i < children.length; ++i) {
    var value = this.__interpretSubtree(children[i]);
    if (i >= 1 && Math.sign(prev.compare(value)) !== expected_sign) {
      return Value.makeFalse();
    }
    prev = value;
  }
  return Value.makeTrue();
};

Interpreter.prototype.__plum = function(node) {
  return this.__comparison(node, 0);
};

Interpreter.prototype.__lesser = function(node) {
  return this.__comparison(node, -1);
};

Interpreter.prototype.__bigger = function(node) {
  return this.__comparison(node, 1);
};

Interpreter.prototype.__toggle = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: #70661E requires exactly one argument");
  }
  return Value.makeBooleanValue(!this.__interpretSubtree(children[0]).isTrue());
};

Interpreter.prototype.__all = function(node) {
  var children = syntaxChildren(node);
  for (var i = 0; i < children.length; ++i) {
    if (!this.__interpretSubtree(children[i]).isTrue()) {
      return Value.makeFalse();
    }
  }
  return Value.makeTrue();
};

Interpreter.prototype.__gold = function(node) {
  var children = syntaxChildren(node);
  for (var i = 0; i < children.length; ++i) {
    var value = this.__interpretSubtree(children[i]);
    if (value.isTrue()) return value;
  }
  return Value.makeFalse();
};

// ============================================================================
// Built-ins: Math
// ============================================================================

// Helper function used by __add, __differ, __dot, __divide, and __residue.
Interpreter.prototype.__leftToRightOp = function(node, nullaryReturn, combine,
                                                 caller) {
  var children = syntaxChildren(node);
  if (children.length === 0) return Value.makeNumericValue(nullaryReturn);
  var result = this.__interpretSubtree(children[0]);
  if (result.type !== "scalar") {
    Interpreter.__oops(node,
        "Unimplemented: non-scalar " + caller + " argument");
  }
  for (var i = 1; i < children.length; ++i) {
    var operand = this.__interpretSubtree(children[i]);
    if (operand.type !== "scalar") {
      Interpreter.__oops(node,
          "Unimplemented: non-scalar " + caller + " argument");
    }
    result.payload = combine(result.payload, operand.payload);
  }
  return result;
};

Interpreter.prototype.__add = function(node) {
  return this.__leftToRightOp(node, /*nullaryReturn=*/ 0,
                              function(a, b) { return a + b }, "#ADD");
};

Interpreter.prototype.__differ = function(node) {
  return this.__leftToRightOp(node, /*nullaryReturn=*/ 0,  // ?
                              function(a, b) { return a - b }, "#D1FFE2");
};

Interpreter.prototype.__dot = function(node) {
  return this.__leftToRightOp(node, /*nullaryReturn=*/ 1,
                              function(a, b) { return a * b }, "#D07");
};

Interpreter.prototype.__divide = function(node) {
  return this.__leftToRightOp(node, /*nullaryReturn=*/ 1,  // ?
                              function(a, b) { return a / (b || 256) },
                              "#D171DE");
};

Interpreter.prototype.__residue = function(node) {
  return this.__leftToRightOp(node, /*nullaryReturn=*/ 1/256,  // TODO: ?!
                              function(a, b) { return a % (b || 256); },
                              "#2E51D0");
};

// This is a little tricky because the arguments are evaluated left to right
// but the exponentiation is done right to left.
Interpreter.prototype.__powderblue = function(node) {
  var children = syntaxChildren(node);
  var args = [];
  for (var i = 0; i < children.length; ++i) {
    var arg = this.__interpretSubtree(children[i]);
    if (arg.type !== "scalar") {
      Interpreter.__oops(node, "Unimplemented: non-scalar powderblue argument");
    }
    args.push(arg);
  }
  var result = Value.makeNumericValue(1);
  while (args.length > 0) {
    result.payload = Math.pow(args.pop().payload, result.payload);  // TODO: NaNs.
  }
  return result;
};

// Helper function used by __log, __abs, __flooor, __sienna, __cos, __tan,
// __moccasin, and __arccos.
Interpreter.prototype.__unaryOp = function(node, operation, caller) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: " + caller + " requires exactly one argument");
  }
  return Value.makeNumericValue(
             operation(this.__interpretSubtree(children[0]).payload));
};

Interpreter.prototype.__log = function(node) {  // TODO: Think about NaNs.
  return this.__unaryOp(node, function(a) { return Math.log(a) }, "#106");
};

Interpreter.prototype.__abs = function(node) {
  return this.__unaryOp(node, function(a) { return Math.abs(a) }, "#AB5");
};

Interpreter.prototype.__flooor = function(node) {
  return this.__unaryOp(node, function(a) { return Math.floor(a) }, "#F10002");
};

Interpreter.prototype.__sienna = function(node) {
  return this.__unaryOp(node, function(a) { return Math.sin(a) }, "sienna");
};

Interpreter.prototype.__cos = function(node) {
  return this.__unaryOp(node, function(a) { return Math.cos(a) }, "#C05");
};

Interpreter.prototype.__tan = function(node) {  // TODO: Think about NaNs.
  return this.__unaryOp(node, function(a) { return Math.tan(a) }, "tan");
};

Interpreter.prototype.__moccasin = function(node) {  // TODO: Think about NaNs.
  return this.__unaryOp(node, function(a) { return Math.asin(a) }, "moccasin");
};

Interpreter.prototype.__arccos = function(node) {  // TODO: Think about NaNs.
  return this.__unaryOp(node, function(a) { return Math.acos(a) }, "#A2CC05");
};

Interpreter.prototype.__arg = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 2) {
    Interpreter.__oops(node,
        "Syntax error: arg requires exactly two arguments");
  }
  var ordinate = this.__interpretSubtree(children[0]);
  var abscissa = this.__interpretSubtree(children[1]);
  return Value.makeNumericValue(Math.atan2(ordinate.payload, abscissa.payload));
};

Interpreter.prototype.__pi = function(node) {
  if (syntaxChildren(node).length > 0) {
    Interpreter.__oops(node, "Syntax error: #314159 requires zero arguments");
  }
  return Value.makeNumericValue(Math.PI);
};

Interpreter.prototype.__e = function(node) {
  if (syntaxChildren(node).length > 0) {
    Interpreter.__oops(node, "Syntax error: #271828 requires zero arguments");
  }
  return Value.makeNumericValue(Math.E);
};

// ============================================================================
// Built-ins: Charts
// ============================================================================

Interpreter.prototype.__coral = function(node) {
  var children = syntaxChildren(node);
  var chart = Value.makeShellValue();
  for (var i = 0; i < children.length; ++i) {
    chart.payload[Value.makeNumericValue(i + 1).pack()] =
        this.__interpretSubtree(children[i]);
  }
  return chart;
};

Interpreter.prototype.__seashell = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: seashell requires exactly one argument");
  }
  var chart = this.__interpretSubtree(children[0]);
  if (chart.type !== "chart" ) {
    Interpreter.__oops(node, "Type error: seashell argument is not a chart");
  }
  return Value.makeBooleanValue(!chart.isTrue());
};

Interpreter.prototype.__navy = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 2) {
    Interpreter.__oops(node,
        "Syntax error: navy requires exactly two arguments");
  }
  var chart = this.__interpretSubtree(children[0]);
  if (chart.type !== "chart") {
    Interpreter.__oops(node,
        "Type error: first navy argument is not a chart");
  }
  var result = chart.payload[this.__interpretSubtree(children[1]).pack()];
  if (!result) {
    Interpreter.__oops(node, "Runtime error: X did not mark the spot");
  }
  return result;
};

Interpreter.prototype.__chartreuse = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 3) {
    Interpreter.__oops(node,
        "Syntax error: chartreuse requires exactly three arguments");
  }
  var chart = this.__interpretSubtree(children[0]);
  if (chart.type !== "chart") {
    Interpreter.__oops(node,
        "Type error: first chartreuse argument is not a chart");
  }
  var x = this.__interpretSubtree(children[1]);
  var value = this.__interpretSubtree(children[2]);
  var copy = chart.clone();
  copy.payload[x.pack()] = value;
  return copy;
};

Interpreter.prototype.__salmon = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: salmon requires exactly one argument");
  }
  var chart = this.__interpretSubtree(children[0]);
  if (chart.type !== "chart") {
    Interpreter.__oops(node, "Type error: salmon argument is not a chart");
  }
  var xs = Object.keys(chart.payload);
  xs.sort(function(a, b) { return -Value.packCompare(a, b) });  // Reverse sort.
  return Value.makeSalmonValue(xs, node.ownerDocument);
};

Interpreter.prototype.__maroon = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 2) {
    Interpreter.__oops(node,
        "Syntax error: maroon requires exactly one argument");
  }
  var chart = this.__interpretSubtree(children[0]);
  if (chart.type !== "chart") {
    Interpreter.__oops(node,
        "Type error: first maroon argument is not a chart");
  }
  var x = this.__interpretSubtree(children[1]);
  var copy = chart.clone();
  delete copy.payload[x.pack()];
  return copy;

};

// ============================================================================
// Built-ins: Strings
// ============================================================================

Interpreter.prototype.__decode = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: #DEC0DE requires exactly one argument");
  }
  var chart = this.__interpretSubtree(children[0]);
  if (chart.type !== "chart") {
    Interpreter.__oops(node, "Type error: #DEC0DE argument is not a chart");
  }
  return Value.makeNumericValue(parseFloat(chart.toOutputString()));
};

Interpreter.prototype.__recode = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: #2EC0DE requires exactly one argument");
  }
  var arg = this.__interpretSubtree(children[0]);
  if (arg.type !== "scalar") {
    Interpreter.__oops(node, "Type error: #2EC0DE argument is not a scalar");
  }
  var s = arg.payload.toString();
  var chart = Value.makeShellValue();
  for (var i = 0; i < s.length; ++i) {
    chart.payload[Value.makeNumericValue(i + 1).pack()] =
        Value.makeNumericValue(s.codePointAt(i));
  }
  return chart;
};

Interpreter.prototype.__ivory = function(node) {
  var children = syntaxChildren(node);
  if (children.length !== 1) {
    Interpreter.__oops(node,
        "Syntax error: ivory requires exactly one argument");
  }
  var arg = this.__interpretSubtree(children[0]);
  return Value.makeBooleanValue(arg.payload === 105 || arg.payload === 118 ||
                                arg.payload === 121);
};

// ============================================================================
// Built-ins: I/O
// ============================================================================

Interpreter.prototype.__get = function(node) {
  if (syntaxChildren(node).length > 0) {
    Interpreter.__oops(node, "Syntax error: #6E7 requires zero arguments");
  }
  if (this.__stdin !== undefined && this.__stdin.length === 0) {
    this.__stdin = window.prompt(
        "Please enter a line of standard input. (Cancel for EOF.)")
    if (this.__stdin === null) {
      this.__stdin = undefined;
    } else {
      this.__stdin += "\n";
    }
  }
  if (this.__stdin === undefined) {
    return Value.makeNumericValue(Color.parseString("#E0F").toNumber());
  }
  var result = this.__stdin.codePointAt(0);
  this.__stdin = this.__stdin.substr(1);
  return Value.makeNumericValue(result);
};

Interpreter.prototype.__output = function(node, prefix) {
  var output = prefix;
  var children = syntaxChildren(node);
  for (var i = 0; i < children.length; ++i) {
    output += this.__interpretSubtree(children[i]).toOutputString();
  }
  alert(output);
  return Value.makeNumericValue(0);  // TODO: What should output return?
};

Interpreter.prototype.__facade = function(node) {
  return this.__output(node, "");
};

Interpreter.prototype.__booboo = function(node) {
  return this.__output(node, "ERROR: ");  // TODO: Do something better?
};

// TODO: This should be better.
Interpreter.prototype.__die = function(node) {
  this.__output(node, "DIE: ");
  throw "Die";
};

// ============================================================================
// Main interpreter functions
// ============================================================================

// Helper function called by __funcArg and __interpretVar to handle values
// read from function arguments and user-defined variables. If the value is a
// fndef, then call the corresponding function. Otherwise verify that it has no
// child nodes and return it.
Interpreter.prototype.__processValue = function(node, value) {
  var children = syntaxChildren(node);
  if (value.type === "fndef") {
    var frame = new StackFrame();
    for (var i = 0; i < children.length; ++i) {
      frame.pushArg(this.__interpretSubtree(children[i]));
    }
    this.__stack.pushFrame(frame);
    var returnValue = this.__interpretSubtree(value.payload,
                                              /*executeFndef=*/true);
    this.__stack.popFrame();
    return returnValue;
  } else {
    if (children.length > 0) {
      Interpreter.__oops(node,
          "Type error: non-function " +
          Color.parseString(node.style.color).nicestName() +
          " cannot have child nodes");
    }
    return value;
  }
};

Interpreter.prototype.__funcArg = function(node, index) {
  var value = this.__stack.fetchArg(index);
  if (!value) {
    Interpreter.__oops(node,
        "Runtime error: undefined function argument " +
        Color.parseString(node.style.color).nicestName());
  }
  return this.__processValue(node, value);
};

Interpreter.prototype.__interpretVar = function(node) {
  var color = Color.parseString(node.style.color);
  var value = this.__stack.fetch(color);
  if (!value) {
    Interpreter.__oops(node,
        "Runtime error: undefined variable " + color.nicestName());
  }
  return this.__processValue(node, value);
};

Interpreter.prototype.__interpretBuiltin = function(node) {
  var c = Color.parseString(node.style.color);
  // Function arguments
  var index = Stack.roygbivIndex(c);
  if (index >= 0) return this.__funcArg(node, index);
  // Control structures
  if (c.equals(Color.honeydew)) return this.__honeydew(node);
  if (c.equals(Color.rgb_1FE15E)) return this.__ifElse(node);
  if (c.equals(Color.teal)) return this.__teal(node);
  if (c.equals(Color.deepskyblue)) return this.__deepskyblue(node);
  if (c.equals(Color.ghostwhite)) return this.__ghostwhite(node);
  if (c.equals(Color.rgb_5CA1A2)) return this.__scalar(node);
  if (c.equals(Color.fuchsia)) return this.__fuchsia(node);
  // Comparisons and Boolean operations
  if (c.equals(Color.plum)) return this.__plum(node);
  if (c.equals(Color.rgb_1E55E2)) return this.__lesser(node);
  if (c.equals(Color.rgb_B166E2)) return this.__bigger(node);
  if (c.equals(Color.rgb_70661E)) return this.__toggle(node);
  if (c.equals(Color.rgb_A11)) return this.__all(node);
  if (c.equals(Color.gold)) return this.__gold(node);
  // Math
  if (c.equals(Color.rgb_ADD)) return this.__add(node);
  if (c.equals(Color.rgb_D1FFE2)) return this.__differ(node);
  if (c.equals(Color.rgb_D07)) return this.__dot(node);
  if (c.equals(Color.rgb_D171DE)) return this.__divide(node);
  if (c.equals(Color.rgb_2E51D0)) return this.__residue(node);
  if (c.equals(Color.powderblue)) return this.__powderblue(node);
  if (c.equals(Color.rgb_106)) return this.__log(node);
  if (c.equals(Color.rgb_AB5)) return this.__abs(node);
  if (c.equals(Color.rgb_F10002)) return this.__flooor(node);
  if (c.equals(Color.sienna)) return this.__sienna(node);
  if (c.equals(Color.rgb_C05)) return this.__cos(node);
  if (c.equals(Color.tan)) return this.__tan(node);
  if (c.equals(Color.moccasin)) return this.__moccasin(node);
  if (c.equals(Color.rgb_A2CC05)) return this.__arccos(node);
  if (c.equals(Color.rgb_A26)) return this.__arg(node);
  if (c.equals(Color.rgb_314159)) return this.__pi(node);
  if (c.equals(Color.rgb_271828)) return this.__e(node);
  // Charts
  if (c.equals(Color.coral)) return this.__coral(node);
  if (c.equals(Color.seashell)) return this.__seashell(node);
  if (c.equals(Color.navy)) return this.__navy(node);
  if (c.equals(Color.chartreuse)) return this.__chartreuse(node);
  if (c.equals(Color.salmon)) return this.__salmon(node);
  if (c.equals(Color.maroon)) return this.__maroon(node);
  // Strings
  if (c.equals(Color.rgb_DEC0DE)) return this.__decode(node);
  if (c.equals(Color.rgb_2EC0DE)) return this.__recode(node);
  if (c.equals(Color.ivory)) return this.__ivory(node);
  // I/O
  if (c.equals(Color.rgb_6E7)) return this.__get(node);
  if (c.equals(Color.rgb_FACADE)) return this.__facade(node);
  if (c.equals(Color.rgb_B00B00)) return this.__booboo(node);
  if (c.equals(Color.rgb_D1E)) return this.__die(node);
  Interpreter.__oops(node,
      "Syntax error: unknown built-in '" + c.nicestName() + "'");
};

// The parameter executeFndef is a boolean; if it is true, then the subtree
// root node is executed even if it is formatted as a function definition (but
// nested function definitions are still just definitions). This is how
// functions are implemented: a function definition is really just an
// assignment of a DOM node to the background color, and when the function is
// called, __interpretSubtree is called again on that DOM node, this time with
// executeFndef set to true, so that the code is actually executed.
Interpreter.prototype.__interpretSubtree = function(node, executeFndef) {
  var fontFamily = unwrapFontName(node.style.fontFamily).toLowerCase();
  if (!fontList.monospace[fontFamily]) {
    var error = "Syntax error: non-monospace font";
    if (fontFamily.indexOf("impact") >= 0) error = error.toUpperCase();
    Interpreter.__oops(node, error);
  }
  var italic = (node.style.fontStyle === "italic");
  var bold = (node.style.fontWeight === "bold");
  var underline = isUnderlined(node);
  var returnValue;
  if (italic && !executeFndef) {
    // Italic without a background color produces an "anonymous function."
    if (!node.id) {
      node.id = 'wysiscript-interpreter-node-' + this.__nextId;
      ++this.__nextId;
    }
    returnValue = Value.makeFndefValue(node);
  } else if (bold && !underline) {  // Built-in operation.
    returnValue = this.__interpretBuiltin(node);
  } else if (underline && !bold) {  // Numeric literal.
    if (syntaxChildren(node).length > 0) {
      Interpreter.__oops(node,
          "Syntax error: numeric literals cannot have arguments");
    }
    returnValue = Value.makeLiteralValue(Color.parseString(node.style.color));
  } else if (!bold && !underline) {  // User-defined variable or function.
    returnValue = this.__interpretVar(node);
  } else {
    Interpreter.__oops(node, "Syntax error: bold + underline");
  }
  if (!executeFndef) {
    var backgroundColor = Color.parseString(node.style.backgroundColor,
                                            /*allowTransparent=*/true);
    if (backgroundColor.creationString !== "transparent") {
      this.__stack.assign(backgroundColor, returnValue);
    }
  }
  return returnValue;
};

Interpreter.prototype.interpret = function(root) {
  this.__stack = new Stack();
  this.__stdin = "";
  var children = syntaxChildren(root);
  try {
    for (var i = 0; i < children.length; ++i) {
      this.__interpretSubtree(children[i], /*executeFndef=*/false);
    }
  } catch (e) {
    alert(e);
  }
};
