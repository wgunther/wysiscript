
function Toolbar(toolbarDiv) {
	this.callBaseMethod(Toolbar);

	this.__container = toolbarDiv;
	this.__actions = {};
	this.__renderToolbarActions();
}

Toolbar.prototype =
{
	updateActionAfterSelection: function(action, toggled) {
		var buttonTypes = ToolbarButton.ButtonType;
		if (Toolbar.ScanningActions.indexOf(action) > -1) {
			this.__actions[action].setValue(toggled);
		}
	},

	widgetAction: function(widgetType, value) {
		var widgetTypes = TaskbarWidget.Widgets;
		var toolbarAction;
		switch (widgetType) {
			case widgetTypes.ForegroundColor:
				toolbarAction = A.ForegroundColor;
			case widgetTypes.BackgroundColor:
				toolbarAction = toolbarAction || A.BackgroundColor;
				this.__actions[toolbarAction].setValue(value);
		}
	},

	ToggleableAction: function(action) {
		var action = this.__actions[action];
		return action.isToggleable;
	},

	isFormatting: function(action) {
		var action = this.__actions[action]
		return action.formatting;
	},

	getValue: function(action, element, style) {
		var action = this.__actions[action];
		if (action) {
			return action.getElementValue(element, style);
		}
	},

	__renderToolbarActions: function () {
		for (var buttonTypeK in ToolbarButton.ButtonType) {
			var buttonType = ToolbarButton.ButtonType[buttonTypeK];
			var button = ToolbarAction.Factory(buttonType, this.__container);
			button.listen(this.__handleAction.bind(this));
			this.__actions[buttonType] = button;
		}
	},

	__handleAction: function(buttonType) {
		var elem = this.__actions[buttonType];
		this._broadcast(buttonType, elem.getValue());
	},

	__showColorDialog: function(action, e) {

		var chooser = new ColorChooser(e.clientX, e.clientY);
		chooser.listen(this.__colorChosen.bind(this, action));
	},

	__colorChosen: function(action, color) {
		this._broadcast(action, color);
	},

}
inherit(Toolbar, Listener);


//#region ToolbarAction

function ToolbarAction(action, container) {
	this.callBaseMethod(ToolbarAction);
	this._action = action;
	this._container = container;

	this.formatting = true;

	this._render();
	this._hookBindings();

	if (this._ui) {
		addCssClass(this._ui, "buttonType" + this._action * 10);
	}
}

ToolbarAction.prototype = {
	isToggleable: false,
	_ui: null,

	getElementValue: function(element, style) {
		return undefined;
	},

	setValue: function (val) {
		this._unhookBindings();
		this._ui.value = val;
		this._hookBindings();
	},

	getValue: function() {
		return this._ui.value;
	},

	_render: function() { },
	_hookBindings: function() { },
	_unhookBindings: function() { },
},

inherit(ToolbarAction, Listener);

ToolbarAction.Factory = function (action, container) {
	switch (action) {
		case A.Bold:
			return new BoldButton(container);
		case A.Italics:
			return new ItalicButton(container);
		case A.Underline:
			return new UnderlineButton(container);
		case A.FontIncrease:
			return new FontIncreaseButton(container);
		case A.FontDecrease:
			return new FontDecreaseButton(container);
		case A.Run:
			return new RunButton(container);
		case A.FontFamily:
			return new FontOptionList(container);
		case A.TextSize:
			return new TextSizeField(container);
		case A.ForegroundColor:
			return new TextColorSelector(container);
		case A.BackgroundColor:
			return new BackgroundColorSelector(container);
	}
};

//#endregion

//#region option list stuff

//#region ToolbarActionOptionList

function ToolbarActionOptionList(action, container) {
	this.callBaseMethod(ToolbarActionOptionList, "", arguments);
}

ToolbarActionOptionList.prototype = {

	_onChange: function() {
		this._broadcast(this._action);
	},

	_render: function() {
		this.callBaseMethod(ToolbarActionOptionList, "_render");
		this._ui = document.createElement("select");
		this._container.appendChild(this._ui);
	},

	_hookBindings: function() {
		this.callBaseMethod(ToolbarActionOptionList, "_hookBindings");
		this.__changeListener = this._onChange.bind(this);
		this._ui.addEventListener("change", this.__changeListener);
	},

	_unhookBindings: function() {
		this.callBaseMethod(ToolbarActionOptionList, "_unhookBindings");
		this._ui.removeEventListener("change", this.__changeListener);
		this.__changeListener = null;
	}
};
inherit(ToolbarActionOptionList, ToolbarAction);

//#endregion

//#region FontOptionList

function FontOptionList(container) {
	this.callBaseMethod(FontOptionList, "", [A.FontFamily, container]);
}

FontOptionList.prototype = {
	getElementValue: function(element, style) {
		return style.getPropertyValue("font-family");
	},

	setValue: function(value) {
		var newVal = unwrapFontName(value);
		newVal = newVal.toLowerCase();
		this.callBaseMethod(FontOptionList, "setValue", [newVal]);
	},

	_render: function() {
		this.callBaseMethod(FontOptionList, "_render");
		this.__fontList = fontList;
		for (var i = 0; i < this.__fontList.fonts.length; ++i) {
			var font = this.__fontList.fonts[i];
			var option = document.createElement("option");
			option.value = font.toLowerCase();
			option.innerText = font;
			this._ui.appendChild(option);
		}
	},
}
inherit(FontOptionList, ToolbarActionOptionList);

//#endregion

//#endregion

//#region field stuff

//#region ToolbarActionField

function ToolbarActionField(action, container) {
	this.callBaseMethod(ToolbarActionField, "", arguments);
}
ToolbarActionField.prototype = {
	__inputListener: null,

	_onInput(e) {
		if (e.key === "Enter") {
			this._broadcast(this._action);
		}
	},

	_render: function() {
		this.callBaseMethod(ToolbarActionField, "_render");
		this._ui = document.createElement("input");
		this._container.appendChild(this._ui);
	},

	_hookBindings: function() {
		this.callBaseMethod(ToolbarActionField, "_hookBindings");
		this.__inputListener = this._onInput.bind(this);
		this._ui.addEventListener("keydown", this.__inputListener);
	},

	_unhookBindings: function() {
		this.callBaseMethod(ToolbarActionField, "_unhookBindings");
		this._ui.removeEventListener("keydown", this.__inputListener);
		this.__inputListener = null;
	}
};
inherit(ToolbarActionField, ToolbarAction);

//#endregion

//#region Text size field

function TextSizeField(container) {
	this.callBaseMethod(TextSizeField, "", [A.TextSize, container]);
}
TextSizeField.prototype = {
	getElementValue: function(element, style) {
		return style.getPropertyValue("font-size");
	},
};

inherit(TextSizeField, ToolbarActionField);

//#endregion

//#endregion

//#region button stuffs

//#region ToolbarActionButton

function ToolbarActionButton(action, container) {
	this.callBaseMethod(ToolbarActionButton, "", arguments);
}
ToolbarActionButton.prototype = {
	__clickListener: null,

	_onClick: function() { 
		this._broadcast(this._action);
	},

	setValue: function(val) {
		var shouldToggleOff = val;

		if (shouldToggleOff === undefined) {
			// if not passed in, we toggle status
			shouldToggleOff = !hasCssClass(this._ui, "toggleOff");
		}

		if (shouldToggleOff) {
			addCssClass(this._ui, "toggleOff");
		}
		else {
			removeCssClass(this._ui, "toggleOff");
		}
	},

	_hookBindings: function() {
		this.__clickListener = this._onClick.bind(this);
		this._ui.addEventListener("click", this.__clickListener);
	},

	_unhookBindings: function() {
		this._ui.removeEventListener("click", this.__clickListener);
		this.__clickListener = null;
	},
	
	_render: function() {
		this._ui = document.createElement("button");
		this._container.appendChild(this._ui);
	},
};
inherit(ToolbarActionButton, ToolbarAction);

//#endregion



//#region Bold Button

function BoldButton(container) {
	this.callBaseMethod(BoldButton, "", [A.Bold, container]);
	this.isToggleable = true;
}
BoldButton.prototype = {
	getElementValue: function(element, style) {
    return isBold(style);
	},

	_render: function() {
		this.callBaseMethod(BoldButton, "_render");
		this._ui.innerText = "B";
		this._ui.style.fontWeight = "bold";
	},
};
inherit(BoldButton, ToolbarActionButton);

//#endregion

// #region Italic Button

function ItalicButton(container) {
	this.callBaseMethod(ItalicButton, "", [A.Italics, container]);
	this.isToggleable = true;
}
ItalicButton.prototype = {
	getElementValue: function(element, style) {
		return style.getPropertyValue("font-style") === "italic";
	},

	_render: function() {
		this.callBaseMethod(ItalicButton, "_render");
		this._ui.innerText = "I";
		this._ui.style.fontStyle = "italic";
	},
};
inherit(ItalicButton, ToolbarActionButton);
//
// #endregion

//#region Underline button

function UnderlineButton(container) {
	this.callBaseMethod(UnderlineButton, "", [A.Underline, container]);
	this.isToggleable = true;
}
UnderlineButton.prototype = {
	getElementValue: function(element, style) {
		return isUnderlined(element);
	},
	_render: function() {
		this.callBaseMethod(UnderlineButton, "_render");
		this._ui.innerText = "U";
		this._ui.style.textDecoration = "underline";
	},
};
inherit(UnderlineButton, ToolbarActionButton);

//#endregion

//#region Font Increase Button

function FontIncreaseButton(container) {
	this.callBaseMethod(FontIncreaseButton, "", [A.FontIncrease, container]);
}
FontIncreaseButton.prototype = {
	_render: function() {
		this.callBaseMethod(FontIncreaseButton, "_render");
		var smallA = document.createElement("span");
		var bigA = document.createElement("span");

		smallA.innerText = "A";
		bigA.innerText = "A";

		smallA.style.fontSize = "8pt";
		bigA.style.marginLeft = "-2px";

		this._ui.appendChild(smallA);
		this._ui.appendChild(bigA);
	},
};
inherit(FontIncreaseButton, ToolbarActionButton);

//#endregion

//#region Font Decrease Button

function FontDecreaseButton(container) {
	this.callBaseMethod(FontDecreaseButton, "", [A.FontDecrease, container]);
}
FontDecreaseButton.prototype = {
	_render: function() {
		this.callBaseMethod(FontDecreaseButton, "_render");
		var smallA = document.createElement("span");
		var bigA = document.createElement("span");

		smallA.innerText = "A";
		bigA.innerText = "A";

		smallA.style.fontSize = "8pt";
		smallA.style.marginLeft = "-2px";

		this._ui.appendChild(bigA);
		this._ui.appendChild(smallA);
	},
};
inherit(FontDecreaseButton, ToolbarActionButton);

//#endregion

//#region Run Button

function RunButton(container) {
	this.callBaseMethod(RunButton, "", [A.Run, container]);
	this.formatting = false;
}

RunButton.prototype = {
	_render: function() {
		this.callBaseMethod(RunButton, "_render");
		var runSVG = getSVG("triangle");
		var svgContainer = document.createElement("span")
		addCssClass(svgContainer, "svgContainer");
		svgContainer.appendChild(runSVG);
		this._ui.appendChild(svgContainer);

		var text = document.createElement("span");
		text.innerText = "Run";
		this._ui.appendChild(text);
	},
};
inherit(RunButton, ToolbarActionButton);

//#endregion

//#endregion

//#region font field
function FontSelector(action, container, defaultColor, caption) {
	FontSelector.lastColors = FontSelector.lastColors || [];
	defaultColor = defaultColor || Color.black;
	this._colorSelector = new ColorSelector(6, 8, defaultColor, FontSelector.lastColors, caption);
	this.callBaseMethod(FontSelector, "", arguments);
	this._colorSelector.listen(this._colorSelectorReturns.bind(this));
}
FontSelector.prototype = {
	_indicator: null,

	_indicatorListener: null,
	_selectorListener: null,

	_colorSelector: null,

	setValue: function (val) {
		val = Color.parseString(val);
		this._colorSelector.colorSelected(val);
	},

	getValue: function() {
		return this._colorSelector.selectedColor;
	},

	_render: function() {
		this.callBaseMethod(FontSelector, "_render");
		
		this._ui = document.createElement("span");

		this._ui.style.display = "flex";

		this._indicator = document.createElement("button");
		this._selector = document.createElement("button");

		this._renderIndicator();	

		this._selector.innerHTML = "\u25BE";
		this._selector.style.fontSize = "10px";
		this._selector.style.width = "12px";

		this._ui.appendChild(this._indicator);
		this._ui.appendChild(this._selector);

		this._container.appendChild(this._ui);

	},

	_hookBindings: function() {
		this._indicatorListener = this.__selectColor.bind(this);
		this._selectorListener = this.__launchIndicator.bind(this);

		this._selector.addEventListener("click", this._selectorListener);
		this._indicator.addEventListener("click", this._indicatorListener);
	},

	_unhookBindings: function() {
		this._selector.removeEventListener("click", this._selectorListener);
		this._indicator.removeEventListener("click", this._indicatorListener);
		this._indicatorListener = null;
		this._selectorListener = null;
	},

	__launchIndicator: function(e) {
		var x = this._indicator.offsetLeft;
		var y = this._indicator.offsetTop + this._indicator.clientHeight;

		var indicatorDiv = document.createElement("div");

		document.body.appendChild(indicatorDiv);

		indicatorDiv.style.position = "absolute";
		indicatorDiv.style.top = y + "px";
		indicatorDiv.style.left = x + "px";

		this._colorSelector.show(indicatorDiv);
	},

	__selectColor: function() {
		if (this._colorSelector.selectedColor) {
			this._broadcast(this._action, this._colorSelector.selectedColor);
		}
	},

	_colorSelectorReturns: function(color) {
		this._renderIndicator();
		this.__selectColor();
	}

};
inherit(FontSelector, ToolbarAction);

function TextColorSelector(container) {
	this.callBaseMethod(TextColorSelector, "", [A.ForegroundColor, container, undefined, "Font color"]);
}

TextColorSelector.prototype = {
	_renderIndicator: function() {
		this._indicator.innerHTML = "";
		
		var text = document.createElement("div");
		text.innerText = "A";
		text.style.margin = "0 15%";
		text.style.borderBottom = "3px solid";
		text.style.borderColor = this._colorSelector.selectedColor;

		this._indicator.appendChild(text);
	},
};
inherit(TextColorSelector, FontSelector);

function BackgroundColorSelector(container) {
	this.callBaseMethod(BackgroundColorSelector, "", [A.BackgroundColor, container, Color.white, "Background color"]);
}
BackgroundColorSelector.prototype = {
	_renderIndicator: function() {
		this._indicator.innerHTML = "";

		var text = document.createElement("div");
		text.innerText = "A";
		text.style.margin = "0 15%";
		text.style.border = "1px solid black";
		var color = this._colorSelector.selectedColor;
		text.style.backgroundColor = color;
		text.style.color = color.contrastColor();

		this._indicator.appendChild(text);
	},
};
inherit(BackgroundColorSelector, FontSelector);

ToolbarButton = {};
ToolbarButton.ButtonType = 
{
	Bold: 1,
	Italics: 2,
	Underline: 3,
	FontFamily: 3.2,
	TextSize: 3.5,
	FontIncrease: 4,
	FontDecrease: 5,
	ForegroundColor: 7,
	BackgroundColor: 7.5,
	Run: 8,
}

A = ToolbarButton.ButtonType;

Toolbar.ScanningActions = [
	A.Bold, A.Italics, A.Underline, A.FontFamily, A.TextSize
]

