function Value(type, payload) {
  this.type = type;
  this.payload = payload;
}

Value.makeNumericValue = function(number) {
  return new Value("scalar", number);
}

Value.makeLiteralValue = function(color) {
  return Value.makeNumericValue(color.toNumber());
};

Value.makeShellValue = function() {
  return new Value("chart", {});
};

Value.makeSalmonValue = function(xs, doc) {
  var chart = Value.makeShellValue();
  for (var i = 0; i < xs.length; ++i) {
    chart.payload[Value.makeNumericValue(i + 1).pack()] =
        Value.unpack(xs[i], doc);
  }
  return chart;
};

Value.makeFndefValue = function(node) {
  if (!node.id) throw "Internal error: makeFndefValue on node without id";
  return new Value("fndef", node);
};

Value.makeTrue = function() {
  return Value.makeNumericValue(1);
};

Value.makeFalse = function() {
  return Value.makeNumericValue(0);
};

Value.makeBooleanValue = function(bool) {
  return bool ? Value.makeTrue() : Value.makeFalse();
};

Value.prototype.clone = function() {
  switch (this.type) {
    case "scalar":
    case "fndef":
      return new Value(this.type, this.payload);
    case "chart":
      var clone = Value.makeShellValue();
      for (var x in this.payload) {
        clone.payload[x] = this.payload[x].clone();
      }
      return clone;
  }
  throw "Oops, forgot to handle type '" + this.type + "' in Value.clone";
};

// TODO: This is really similar to pack. Do we need both?
Value.prototype.toString = function() {
  switch (this.type) {
    case "scalar":
      return this.payload.toString();
    case "chart":
      var string = "[";
      var xs = Object.keys(this.payload);
      // Note: This is the opposite sort order from that used by salmon.
      xs.sort(function(a, b) { return Value.packCompare(a, b) });
      for (var i = 0; i < xs.length; ++i) {
        if (i > 0) string += ", ";
        string += Value.unpack(xs[i]) + ": " + this.payload[xs[i]];
      }
      return string + "]";
    case "fndef":
      // TODO: Something better here?
      return "<" + this.payload.textContent + ">";
  }
  throw "Oops, forgot to handle type '" + this.type +
        "' in Value.prototype.toString";
};

Value.prototype.toOutputString = function() {
  switch (this.type) {
    case "scalar":
      return this.payload.toString();
    case "chart":
      var string = "";
      var xs = Object.keys(this.payload);
      xs.sort(function(a, b) { return Value.packCompare(a, b) });
      for (var i = 0; i < xs.length; ++i) {
        var v = this.payload[xs[i]];
        if (v.type !== "scalar") {
          throw "Unimplemented: output for non-scalar chart elements";
        }
        string += String.fromCodePoint(Math.round(v.payload));
      }
      return string;
    case "fndef":
      throw "Unimplemented: output for fndefs";
  }
  throw "Oops, forgot to handle type '" + this.type +
        "' in Value.prototype.toOutputString";
};

Value.prototype.isTrue = function() {
  switch (this.type) {
    case "scalar":
      return this.payload !== 0;
    case "chart":
      for (var x in this.payload) return true;
      return false;
    case "fndef":
      return true;
  }
  throw "Oops, forgot to handle type '" + this.type +
        "' in Value.prototype.isTrue";
};

// Used by built-ins plum, #1E55E2, and #B166E2.
Value.prototype.compare = function(that) {
  switch (this.type) {
    case "scalar":
      if (that.type !== "scalar") return +1;
      return this.payload - that.payload;
    case "chart":
      if (that.type === "fndef") return +1;
      if (that.type === "scalar") return -1;
      var thisXs = Object.keys(this.payload);
      thisXs.sort(function(a, b) { return Value.packCompare(a, b) });
      var thatXs = Object.keys(that.payload);
      thatXs.sort(function(a, b) { return Value.packCompare(a, b) });
      for (var i = 0; ; ++i) {
        if (i >= thisXs.length && i >= thatXs.length) return 0;
        if (i >= thisXs.length) return -1;
        if (i >= thatXs.length) return +1;
        var cmp = Value.packCompare(thisXs[i], thatXs[i]) ||
                  this.payload[thisXs[i]].compare(that.payload[thatXs[i]]);
        if (cmp) return cmp;
      }
    case "fndef":
      if (that.type !== "fndef") return -1;
      return this.payload.id.localeCompare(that.payload.id);  // TODO: ?
  }
  throw "Oops, forgot to handle type '" + this.type + "' in Value.compare";
};

// Pack a value as a string for use as a chart X.
Value.prototype.pack = function() {
  switch (this.type) {
    case "scalar":
      // TODO: Pack-unpack probably don't preserve floating-point values
      // exactly.
      return "s" + this.payload;
    case "chart":
      var packed = "[";
      var xs = Object.keys(this.payload);
      // Note: This is the opposite sort order from that used by salmon.
      xs.sort(function(a, b) { return Value.packCompare(a, b) });
      for (var i = 0; i < xs.length; ++i) {
        if (i > 0) packed += ",";
        packed += xs[i] + ":" + this.payload[xs[i]].pack();
      }
      return packed + "]";
    case "fndef":
      return "f" + this.payload.id;
  }
  throw "Oops, forgot to handle type '" + this.type +
        "' in Value.prototype.pack";
};

// Given a packed chart and the starting index of a key-value pair, returns the
// packed key and packed value and the starting index of the next key-value
// pair (which is not necessarily a valid index into the packed string, but is
// valid for another call to __parseKv). To parse a packed chart, the first
// call should be __parseKv(packed, 1), because a packed chart begins with the
// character '[', and afterward calls to __parseKv should use the previously
// returned nextKvStart. Returns null when there are no more key-value pairs.
Value.__parseKv = function(packed, start) {
  if (start >= packed.length - 1) {
    return null;
  }
  var nestedDepth = 0, key;
  for (var i = start; i < packed.length; ++i) {
    var c = packed.substr(i, 1);
    switch (c) {
      case ":":
        if (nestedDepth === 0) {
          key = packed.substring(start, i);
          start = i + 1;
        }
        break;
      case ",":
      case "]":
        if (nestedDepth === 0) {
          return { key: key, value: packed.substring(start, i),
                   nextKvStart: i + 1 };
        }
        if (c === "]") --nestedDepth;
        break;
      case "[":
        ++nestedDepth;
        break;
    }
  }
  throw "Internal error: parseKv('" + packed + "', " + start + ")";
};

Value.unpack = function(packed, doc) {
  switch (packed.substr(0, 1)) {
    case "s":  // scalar
      return Value.makeNumericValue(parseFloat(packed.substr(1)));
    case "[":  // chart
      var chart = Value.makeShellValue();
      var kvStart = 1;
      for (;;) {
        var kv = Value.__parseKv(packed, kvStart);
        if (!kv) break;
        chart.payload[kv.key] = Value.unpack(kv.value);
        kvStart = kv.nextKvStart;
      }
      return chart;
    case "f":  // fndef
      return Value.makeFndefValue(doc.getElementById(packed.substr(1)));
  }
  throw "Unrecognized pack type: '" + packed.substr(0, 1) + "'";
};

// Linear ordering: first by type (fndef < chart < scalar), then within type.
// This order maximizes the usefulness of salmon, which returns the X's of a
// chart in reverse order (so scalars are first, then charts, then fndefs).
// Scalars are ordered by numeric value. Charts are compared lexicographically
// by key-value pairs, and key-value pairs are compared first by key and then
// by value. So if the first key of a (where "first" is defined recursively) is
// different from the first key of b, then the result of that key comparison is
// returned; else if the corresponding value in a is different from the
// corresponding value in b, then the result of that value comparison is
// returned. Otherwise the comparison moves on to the next key-value pair in
// each. If one of the charts runs out of key-value pairs before the other,
// that chart is less than the other.
Value.packCompare = function(a, b) {
  var bSigil = b.substr(0, 1);
  switch (a.substr(0, 1)) {
    case "s":  // scalar
      if (bSigil !== "s") return +1;
      return parseFloat(a.substr(1)) - parseFloat(b.substr(1));
    case "[":  // chart
      if (bSigil === "f") return +1;
      if (bSigil === "s") return -1;
      var aKvStart = 1;
      var bKvStart = 1;
      for (;;) {
        var aKv = Value.__parseKv(a, aKvStart);
        var bKv = Value.__parseKv(b, bKvStart);
        if (!aKv && !bKv) return 0;
        if (!aKv) return -1;
        if (!bKv) return +1;
        var cmp = Value.packCompare(aKv.key, bKv.key) ||
                  Value.packCompare(aKv.value, bKv.value);
        if (cmp) return cmp;
        aKvStart = aKv.nextKvStart;
        bKvStart = bKv.nextKvStart;
      }
    case "f":  // fndef
      if (bSigil !== "f") return -1;
      return a.substr(1).localeCompare(b.substr(1));  // TODO: something better?
  }
  throw "Unrecognized pack type: '" + a.substr(0, 1) + "'";
};
