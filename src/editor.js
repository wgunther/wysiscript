
function Editor(editorDiv, toolbar, taskbar, linter, texter, interpreter) {
	this.callBaseMethod(Editor);

	this.__undos = [];
	this.__redos = [];

	this.__container = editorDiv;
	this.__toolbar = toolbar;
	this.__taskbar = taskbar;
	this.__linter = linter;
	this.__texter = texter;
	this.__interpreter = interpreter;

	this.__window = getWindow(editorDiv);
	this.__document = getDocument(editorDiv);

	this.doc = this.__document;

	this.__render();
	this.__setupBindings();
	this.__container.focus();
}

Editor.prototype = 
{

//#region fields
	
	//<field isDOM="true">the editor div</field>
	__container: null,
	//<field type="Toolbar">toolbar object</field>
	__toolbar: null,
	//<field type="Linter">Linter object</field>
	__linter: null,
	//<field type="Texter">Syntax texter object</field>
	__texter: null,
	
	//<field type="Window">window of the editor iframe</field>
	__window: null,
	//<field type="HTMLDocument">document of the editor iframe</field>
	__document: null,
	
	//<field type="Array">if populated, formats that should be done on the next character typed by user</field>
	__queuedFormats: null,

//#endregion

//#region Public methods

	lint: function(explicit) {
		if (explicit || Settings.autolint) {
			this.__linter.lint(this.__container);
			if (Settings.autotext) {
				this.__texter.text(this.__container);
			}
		}
	},

	text: function(explicit) {
		this.lint();
		this.__texter.text(this.__container);
	},

	run: function() {
		this.lint();
		this.__interpreter.interpret(this.__container);
	},
	
	makeNew: function() {
		this.__container.innerHTML = "";
	},
	
	display: function(contents) {
		this.__container.innerHTML = contents;
		this.__container.focus();
	},

	blob: function() {
		this.lint();

		var emptySpan = document.createElement("span");
		addCssClass(emptySpan, "version");
		emptySpan.dataset["version"] = Settings.version;
		this.__container.appendChild(emptySpan);

		var contents = this.__container.innerHTML;
		var version = Settings.version;

		var blob = new Blob([contents], {type: 'text/plain'});
		return blob;
	},

	undo: function() {
		if (this.__undos.length) {
			this.__addRedo(this.__container.innerHTML);
			var html = this.__undos.pop();
			this.display(html);
		}
	},

	redo: function() {
		if (this.__redos.length) {
			this.__addUndo(this.__container.innerHTML);
			var html = this.__redos.pop();
			this.display(html);
		}
	},

//#endregion

//#region Private methods

//#region Event listeners

	__onKeyDown: function(e) {
		///<summary>Listens to keydown in edtiro</summary>
		//<param name="e" type="Event">Event information</param>
		
		var queuedFormats = this.__queuedFormats;
		this.__clearQueuedFormat();

		if (e.key.length === 1 && !queuedFormats) {
			// no need to handle typical case
			return;
		}

		var sel = new Selection(this.__container);

		if (!sel.justCursor()) {
			// for real selections, ya know, don't do stuff
			return;
		}
		
		if (e.altKey || e.ctrlKey || e.metaKey) {
			// lets not do anything special here
			return;
		}


		var textToAdd;

		if (e.key.length === 1) {
			// this probably covers our bases with single character
			// not sure how cross browser
			textToAdd = e.key;
		} else {
			switch (e.keyCode) {
				case 13: // enter
					textToAdd = "\n\r";
					break;
				case 9: // tab
					textToAdd = "\t";
					break;
				default:
					return;
			}
		}

		if (!textToAdd) {
			// nothing to add, let's just bail
			return;
		}

		var info = this.__scanSelection();
		var selection = info.sel.range;
		var node = selection.startContainer;

		var newElement = this.__makeTextSpan(textToAdd);

		if (node !== this.__container) {
			// we break the text node our cursor is in into pieces
			var nodes = this.__breakTextNode(node, selection.startOffset, selection.endOffset);
			if (nodes[2]) {
				// if there's a right node, put the new element before that one
				var rightNode = nodes[2];
				rightNode.parentNode.insertBefore(newElement, rightNode);
			}
			else {
				// otherwise, put it after
				var leftNode = nodes[0];
				leftNode.parentNode.insertBefore(newElement, leftNode.nextSibling);
			}

		} else {
			this.__container.appendChild(newElement);
		}

		if (queuedFormats) {
			for (var action in queuedFormats) {
				this.__formatElementBase(newElement, +action, queuedFormats[action]);
			}
		}
		this.lint();
		info.sel.addText(textToAdd.length);
		info.sel.restoreFromTextPosition(info);
		e.preventDefault();
	},

	__onSelectionChanged: function() {
		///<summary>Listens to summary change events</summary>
		var info = this.__scanSelection();

		var scanningActions = Toolbar.ScanningActions;
		var action;
		for (var i = 0; i < scanningActions.length; ++i) {
			action = scanningActions[i];
			this.__toolbar.updateActionAfterSelection(action, info.all[action]);
		}

		if (this.__didSelectionChange(info)) {
			this.__clearQueuedFormat();
		}

		for (action in this.__queuedFormats) {
			this.__toolbar.updateActionAfterSelection(+action, this.__queuedFormats[action]);
		}
		this.__lastInfo = info;
	},

	__onToolbarAction: function(action, args) {
		///<summary>Listens to toolbar to determine which actions are required on editor</summary>
		//<param name="action" type="ToolbarButton.ButtonType">They kind of action</param>
		//<param name="args" type="Object" optional="true">arguments </param>

		var toolbarActions = ToolbarButton.ButtonType;
		var info = this.__scanSelection();

		var beforeChange = this.__container.innerHTML;

		var formattingDone = false;

		if (this.__toolbar.isFormatting(action)) {
				formattingDone = this.__executeToolbarAction(action, args, info);
		}
		else {
			switch (action) {
				case toolbarActions.Run:
					this.run();
					break;
				default:
					throw "Uh oh";
			}
		}
			
		info.lint = info.lint;

		if (formattingDone) {
			this.__addUndo(beforeChange);
			this.__clearRedos();
			this.lint();
			info.sel.restoreFromTextPosition();
		}
		else {
			this.__onSelectionChanged();
		}

		this.__container.focus();

	},
	

//#endregion

//#region initialization

	__render: function () {
		//<summary>called upon construction: sets up the editor's HTML structure</summary>
		var container = this.__container;
		container.contentEditable = true;
		container.spellcheck = false;
	},

	__setupBindings: function() {
		//<summary>sets up any bindings</summary>
		var toolbar = this.__toolbar;
		toolbar.listen(this.__onToolbarAction.bind(this));

		this.__document.addEventListener("selectionchange", this.__onSelectionChanged.bind(this));
		this.__container.addEventListener("keydown", this.__onKeyDown.bind(this));

	},

//#endregion

//#region selection management

	__didSelectionChange: function(info) {
		if (!this.__lastInfo) { return true; }
		if (!info) { return true; }
		return !info.sel.isSameSelection(this.__lastInfo.sel);
	},

	__updateInfo: function(element, style, scanningActions, info) {
		for (var i = 0; i < scanningActions.length; ++i) {
			var action = scanningActions[i];
			
			var value = this.__toolbar.getValue(action, element, style);
			setFirstClearIfDifferent(value, info.all, action);
		}
	},

	__scanSelection: function() {
		///<summary>gathers selection information</summary>
		//<returns type="Object">info about selection</return>
		var actions = ToolbarButton.ButtonType;
		var all = { };
		
		var scanningActions = Toolbar.ScanningActions;
		var widgets = TaskbarWidget.Widgets;

		var info = {
			all: all,
			widget: {},
			fontsizes: [],
		};

		// first get information about the selected nodes
		var sel = new Selection(this.__container);
		sel.traverseSelection(function(node, sOffset, eOffset, num) {
			var element;
			if (isElement(node)) {
				element = node;
			}
			else {
				element = node.parentElement;
			}
			
			var style = window.getComputedStyle(element);

			this.__updateInfo(element, style, scanningActions, info);
			this.__taskbar.notifySelecting(element, style, info.widget);
			
			// we also keep track of font sizes for increasing/decreasing font sizes
			info.fontsizes.push(style.getPropertyValue("font-size"));
		}.bind(this));

		this.__taskbar.notifySelectingComplete(info.widget);

		sel.analyzeSelectionTextPosition();
		info.sel = sel;

		return info;
	},

//#endregion

//#region undo/redo

	__addUndo: function(html) {
		this.__undos = this.__undos || [];
		this.__undos.push(html);
	},
	

	__clearUndos: function(html) {
		this.__undos = [];
	},

	__addRedo: function(html) {
		this.__redos = this.__redos || [];
		this.__redos.push(html);
	},

	__clearRedos: function(html) {
		this.__redos = [];
	},

//#endregion

//#region formatting

	__formatElementBase: function (element, action, arg) {
		//<summary>This function governs how every toolbar action that formats elements formats an element</summary>
		//<param name="element" isdom="true">element to format</param>
		//<param name="action" type="Toolbar.ToolbarAction">action</param>
		//<param name="arg" type="Object">Some argument telling us how to format (action specific)</param>
		var toolbarActions = ToolbarButton.ButtonType;
		switch (action) {
			case toolbarActions.Bold:
				if (arg) {
					element.style.fontWeight = "bold";
				}
				else {
					element.style.fontWeight = "normal";
				}
				break;
			case toolbarActions.Italics:
				if (arg) {
					element.style.fontStyle = "italic";
				}
				else {
					element.style.fontStyle = "normal";
				}
				break;
			case toolbarActions.Underline:
				if (arg) {
					element.style.textDecoration = "underline";
				}
				else {
					element.style.textDecoration = "none";
				}
			case toolbarActions.ForegroundColor:
				element.style.color = arg;
				break;
			case toolbarActions.BackgroundColor:
				element.style.backgroundColor = arg;
				break;
			case toolbarActions.TextSize:
				element.style.fontSize = arg;
				break;
			case toolbarActions.FontFamily:
				element.style.fontFamily = wrapFontName(arg);

		}
	},

	__formatElement: function (action, args, info, element, num) {
		///<summary>uses selection info to format</summary>
		//<param name="action" type="Toolbar.ToolbarAction">action</param>
		//<param name="args" type="">argument corresponding to action</param>
		//<param name="info" type="Object">selection information</param>
		//<param name="element" isdom="true">element to format</param>
		//<param name="num" type="Number">node number</param>
		//<returns></return>
		var toolbarActions = ToolbarButton.ButtonType;
		
		var value;

		switch (action) {
			case toolbarActions.Bold:
			case toolbarActions.Italics:
			case toolbarActions.Underline:
				value = !info.all[action]; // Toggle these.
				break;
			case toolbarActions.ForegroundColor:
			case toolbarActions.BackgroundColor:
			case toolbarActions.TextSize:
			case toolbarActions.FontFamily:
				value = args;
				break;
			case toolbarActions.FontIncrease:
				action = toolbarActions.TextSize;
				value = scaleFontSize(info.fontsizes[num], 1.25)
				break;
			case toolbarActions.FontDecrease:
				action = toolbarActions.TextSize;
				value = scaleFontSize(info.fontsizes[num], 1/1.25)
				break;
		}
		this.__formatElementBase(element, action, value)
	},

	__executeToolbarAction: function(action, args, info) {
		///<summary>does a toolbar action</summary>
		///<param name="action" type="ToolbarButton.ButtonType">action to perform</param>
		///<param name="args" type="Object">argument to how should be formatting</param>
		///<param name="info" type="Object">information about selection</param>
		///<returns type="Boolean">Whether formatting was done which would necessitate linting</returns>

		if (info.sel.justCursor()) {
			// if there's no selection, we want to queue up an action on next keystroke
			this.__queueFormattingChange(action, args, info);
			return false;
		}
		else {
			this.__formatSelectedRegion(action, args, info);
			return true;
		}
	},

	__formatSelectedRegion: function(action, args, info) {
		///<summary>formats a selected region based on what we have analyzed about the selection and any parameters</summary>
		///<param name="action" type="ToolbarButton.ButtonType">action to perform</param>
		///<param name="args" type="Object">argument to how should be formatting</param>
		///<param name="info" type="Object">information about selection</param>

		var sel = info.sel;

		sel.traverseSelection(function(node, sOffset, eOffset, num) {
			if (!isElement(node)) {
				// this is the case we're in a text node that begins or
				// ends the selection. We need to break it first
				var nodes = this.__breakTextNode(node, sOffset, eOffset);
				// nodes[1] is the piece that contains that offset piece
				this.__formatElement(action, args, info, nodes[1], num)
			}
			else {
				this.__formatElement(action, args, info, node, num);
			}
		}.bind(this));
	},

	__getElemToFormat: function(node, sOffset, eOffset) {
		///<summary></summary>
		//<param name="node" type="HTMLNode">node</param>
		//<param name="sOffset" type="Number">starting text offset</param>
		//<param name="Offset" type="Number">ending text offset</param>
		//<returns isDOM="true">a DOM element to format</return>

		if (isElement(node)) {
			return node;
		}
		var textDisplayed = node.textContent;

		if (sOffset === undefined) {
			sOffset = 0;
		}
		if (eOffset === undefined) {
			eOffset = textDisplayed.length;
		}

		if (sOffset === 0 && eOffset === textDisplayed.length) {
			// entire node highlighted!
			var par = node.parentElement;
		}
		return node;
	},

//#endregion

//#region queued action handling

	__addQueuedFormat: function (action, value) {
		///<summary>adds something to be queued to be done</summary>
		//<param name="action" type="ToolbarButton.ButtonType">type of format</param>
		//<param name="value" type="Object">value to change format to</param>
		this.__queuedFormats = this.__queuedFormats || {};
		if (this.__queuedFormats[action] !== undefined) {
			if (this.__queuedFormats[action] === !!this.__queuedFormats[action]) {
				this.__queuedFormats[action] = !this.__queuedFormats[action];
				return;
			}
		}
		this.__queuedFormats[action] = value;
	},

	__clearQueuedFormat: function () {
		///<summary>clears the queued formatting</summary>
		this.__queuedFormats = undefined;
	},

	__queueFormattingChange: function (action, args, info) {
		///<summary>uses selection information to decide what kind of queued change should be added</summary>
		//<param name="action" type="ToolbarButton.ButtonType">type of format</param>
		//<param name="args" type="Object">argument (specific to action)</param>
		//<param name="info" type="Object">selection information</param>
		var val;
		if (this.__toolbar.ToggleableAction(action)) {
			val = !info.all[action];
		} else {
			val = args;
		}
		this.__addQueuedFormat(action, val);
	},
//
//#endregion

//#region some misc

	__breakTextNode: function(node, sOffset, eOffset) {
		///<summary>breaks a text node into up to 3 based on offsets</summary>
		//<param name="node" isDOM="true">text node to break</summary>
		//<param name="sOffset" type="Number">starting offset for where to break</param>
		//<param name="eOffset" type="Number">ending offset for where to break</param>
		//<returns type="Array">An array; at index 0 will be the left piece, 1 the middle
		// (specified by offsets), 2 the right.</returns>
		var textContent = node.textContent;

		var len = textContent.length;

		if (sOffset === undefined) {
			sOffset = 0;
		}
		if (eOffset === undefined) {
			eOffset = len;
		}

		var text = [];
		text[0] = textContent.substring(0, sOffset);
		text[1] = textContent.substring(sOffset, eOffset);
		text[2] = textContent.substring(eOffset, len);

		var nodes = [];
		var container = document.createElement("span");
		replaceNode(node, container);
		for (var i = 0; i < text.length; ++i) {
			if (text[i]) {
				nodes[i] = this.__makeTextSpan(text[i]);
				container.appendChild(nodes[i]);
			}
		}
		return nodes;
	},

	__makeTextSpan: function (text) {
		///<summary>make a span containing text node</summary>
		//<param name="text" type="String">string to add to span</param>
		//<returns isDOM="true">span containing node</return>
		var span = document.createElement("span");
		var text = document.createTextNode(text);
		span.appendChild(text);
		return span;
	},

//#endregion

//#endregion

}

inherit(Editor, Listener);
