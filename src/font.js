function FontList() {
	this.fonts = [];
	this.monospace = {};
	for (var i = 0; i < FontList.testFonts.length; ++i) {
		var font = new Font(FontList.testFonts[i]);
		if (font.isFont) {
			this.fonts.push(font.fontName);
			if (font.isFixedWidth) {
			  this.monospace[font.fontName.toLowerCase()] = 1;
			}
		}
	}
}

FontList.testFonts = [
  "Adobe Jenson", "Agency FB", "Akzidenz-Grotesk", "Albertus", "Aldus",
  "Alexandria", "Algerian", "American Scribe", "American Typewriter",
  "AMS Euler", "Andale Mono", "Andal\u00e9 Sans", "Andy", "Antiqua",
  "Antique Olive", "Apple Chancery", "Apple Color Emoji", "Archer", "Arial",
  "Arial Black", "Arial Monospaced", "Arial Narrow", "Arial Unicode MS",
  "Arno", "Ashley Script", "Aster", "Athens", "Aurora", "Avant Garde Gothic",
  "Avenir", "Balloon", "Bank Gothic", "Baskerville", "Baskerville Old Face",
  "Bastard", "Batang", "Bauer Bodoni", "Bauhaus", "Bauhaus 93", "Bell",
  "Bell Centennial", "Bell Gothic", "Belwe Roman", "Bembo", "Benguiat Gothic",
  "Berkeley Old Style", "Berlin Sans", "Bernhard Modern", "Bitstream Vera",
  "Bodoni", "Book Antiqua", "Bookman", "Bookman Old Style", "Bradley Hand",
  "Brandon Grotesque", "Breitkopf Fraktur", "Britannic Bold", "Brush Script",
  "Brush Script MT", "Bulmer", "Caledonia", "Calibri", "Californian FB",
  "Calisto MT", "Cambria", "Candara", "Candida", "Capitals", "Cartier",
  "Casey", "Caslon", "Caslon Antique", "Catull", "Centaur", "Century Gothic",
  "Century Old Style", "Century Schoolbook", "Century Schoolbook Infant",
  "C\u00e9zanne", "Chalkboard", "Chalkduster", "Charcoal", "Charis SIL",
  "Charter", "Cheltenham", "Chicago", "Chiller", "Choc", "Cholla Slab", "City",
  "Clarendon", "Clearface", "Clearview", "Cloister Black", "Cochin", "Colonna",
  "Colonna MT", "Comic Sans", "Comic Sans MS", "Compacta", "Computer Modern",
  "Concrete Roman", "Consolas", "Constantia", "Cooper Black", "Copperplate",
  "Copperplate Gothic", "Corbel", "Corona", "Courier", "Courier New",
  "Curlz MT", "DejaVu Sans", "DejaVu Sans Mono", "DejaVu Serif", "Desdemona",
  "Didot", "DIN", "Dom Casual", "Dotum", "Droid Sans", "Droid Sans Mono",
  "Droid Serif", "Dyslexie", "Ecofont", "Edwardian Script ITC", "Egyptienne",
  "Elephant", "Emerson", "Engravers MT", "Eras", "Espy Sans", "Esseltub",
  "Eurocrat", "Eurostile", "Everson Mono", "Everson Mono Unicode", "Excelsior",
  "Fairfield", "Fette Fraktur", "FF Dax", "FF Meta", "FF Scala",
  "FF Scala Sans", "Fifteenth Century", "Fira Sans", "Fixed", "Fixedsys",
  "Fixedsys Excelsior", "Fletcher", "Folio", "Footlight", "Forte", "Fraktur",
  "Franklin Gothic", "Franklin Gothic Book", "FreeSans", "FreeSerif",
  "French Script", "Friz Quadrata", "Frutiger", "Futura", "Garamond", "Geneva",
  "Gentium", "Georgia", "Gill Sans", "Gill Sans Schoolbook", "Gloucester",
  "Gotham", "Goudy", "Goudy Old Style", "Granjon", "Guardian Egyptian",
  "Haettenschweiler", "Handel Gothic", "Hei", "Helvetica", "Helvetica Neue",
  "Herculanum", "High Tower Text", "Highway Gothic", "Hobo", "Hoefler Text",
  "HyperFont", "Impact", "Imprint", "Inconsolata", "Industria", "Interstate",
  "Ionic No. 5", "ITC Benguiat", "ITC Zapf Chancery", "Janson", "Joanna",
  "Johnston/New Johnston", "Jokerman", "Kabel", "Klavika", "Korinna",
  "Kristen", "Kuenstler Script", "Latin Modern Mono", "Latin Modern Mono Caps",
  "Letter Gothic", "Lexia", "Lexia Readable", "Lexicon", "Liberation Mono",
  "Liberation Sans", "Liberation Serif", "Linux Biolinum", "Linux Libertine",
  "Literaturnaya", "Lucida Blackletter", "Lucida Bright", "Lucida Calligraphy",
  "Lucida Console", "Lucida Fax", "Lucida Grande", "Lucida Handwriting",
  "Lucida Sans", "Lucida Sans Typewriter", "Lucida Sans Unicode",
  "Lucida Typewriter", "Lydian", "Marion", "Marker Felt", "Meiryo", "Melior",
  "Memphis", "Menlo", "Meta", "MICR", "Microgramma", "Microsoft Sans Serif",
  "Miller", "Minion", "Mistral", "Modern", "Modern No. 20", "Mona Lisa",
  "Monaco", "Monospace", "Monotype Corsiva", "Motorway", "Mrs Eaves",
  "MS Gothic", "MS Mincho", "MS Sans Serif", "MS Serif", "Myriad",
  "Neutraface", "Neuzeit S", "New Century Schoolbook", "New York", "News 701",
  "News 702", "News 705", "News 706", "News Gothic", "News Gothic MT",
  "Nilland", "Nimbus Mono L", "Nimbus Roman", "Nimbus Sans L", "Noteworthy",
  "NPS Rawlinson Roadway", "OCR A Extended", "Old English Text",
  "Old English Text MT", "Onyx", "Open Sans", "Optima", "Osaka", "Palatino",
  "Papyrus", "Parisine", "PCMyungjo", "Perpetua", "Perpetua Titling MT",
  "Phosphate", "Plantin", "Playbill", "PragmataPro", "Prestige",
  "Prestige Elite", "Primer", "Product Sans", "ProFont", "Proxima Nova",
  "PT Mono", "PT Sans", "PT Serif", "Rail Alphabet", "Renault", "Requiem",
  "Roboto", "Roboto Slab", "Rockwell", "Rotis Sans", "Rotis Serif", "Sabon",
  "Sans-serif", "Schadow", "Schwabacher", "Segoe Script", "Segoe UI", "Serifa",
  "SimHei", "SimSun", "Sistina", "Skeleton Antique", "Skia", "Slab serif",
  "Source Code Pro", "Source Sans Pro", "Souvenir", "Square 721", "Sreda",
  "Stencil", "STIX", "Sweden Sans", "Swift", "Swiss 721", "Sylfaen", "Symbol",
  "Syntax", "Tahoma", "Template Gothic", "Terminal", "Thesis Sans", "Times",
  "Times New Roman", "Tiresias", "Torino", "Tower", "Trade Gothic", "Trajan",
  "Transport", "Trebuchet MS", "Trinit\u00e9", "Trixie", "Trump Mediaeval",
  "Tw Cen MT", "Twentieth Century", "Ubuntu", "Ubuntu Mono", "Univers",
  "Utopia", "Vera Sans", "Vera Sans Mono", "Vera Serif", "Verdana", "Webdings",
  "Wide Latin", "Windsor", "Wingdings", "Wingdings 2", "Wingdings 3", "Wyld",
  "XITS", "Zapf Dingbats", "Zapfino", "Zurich",
];

function Font(fontName) {
	this.fontName = fontName;
	this.__testDiv = document.getElementById("test");
	if (!Font.initialized) {
		Font.initialize(this.__testDiv);
	}
	this.isFont = false;
	this.isFixedWidth = false;
	this.__testFont();
}

Font.initialize = function (testDiv) {
	/* compute some constants */
    testDiv.style.fontSize = Font.testSize;
    testDiv.innerText = Font.testString;
    var defaultWidth = {};
    var defaultHeight = {};

    for (var i = 0; i < Font.baseFonts.length; ++i) {
        testDiv.style.fontFamily = Font.baseFonts[i];
        defaultWidth[Font.baseFonts[i]] = testDiv.offsetWidth;
        defaultHeight[Font.baseFonts[i]] = testDiv.offsetHeight;
    }

	testDiv.innerText = "";
	Font.defaultWidth = defaultWidth;
	Font.defaultHeight = defaultHeight;
	Font.initialized = true;
}
/* Test inspired by: http://stackoverflow.com/questions/3368837/list-every-font-a-users-browser-can-display/3368855#3368855 */

Font.testString  = "MMMmmlli";
Font.testString2 = "1iillnmm";
Font.testSize = "100px";
Font.baseFonts = ['monospace', 'sans-serif', 'serif'];

Font.prototype = {
	__testFont: function() {
		var testDiv = this.__testDiv;
		var font = this.fontName;
		testDiv.innerText = Font.testString;
        for (var i = 0; i < Font.baseFonts.length; ++i) {
            testDiv.style.fontFamily = font + ',' + Font.baseFonts[i];
            var matched = (testDiv.offsetWidth != Font.defaultWidth[Font.baseFonts[i]] || testDiv.offsetHeight != Font.defaultHeight[Font.baseFonts[i]]);
			if (matched) {
				this.isFont = true;
				break;
			}
        }

		if (this.isFont) {
			// now, let's see if it's monospaced
			testDiv.style.fontFamily = font;
			var width1 = testDiv.offsetWidth;
			testDiv.innerText = Font.testString + Font.testString2;
			var width2 = testDiv.offsetWidth;

			this.__diff = Math.abs(width1 * 2 - width2);
			if (this.__diff <= 1) {
				this.isFixedWidth = true;
			}
		}

		testDiv.innerText = "";

	}
};
