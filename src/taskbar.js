
function Taskbar(container, toolbar) {
	this.__container = container;
	this.__widgets = [];
	this.__toolbar = toolbar;
	this.__render();
}

Taskbar.prototype = {
	notifySelecting: function(element, style, cache) {
		this.__foreachWidget(function(widget) {
			widget.notifySelecting(element, style, cache);
		});
	},

	notifySelectingComplete: function(cache) {
		this.__foreachWidget(function(widget) {
			widget.notifySelectingComplete(cache);
		}.bind(this));
	},

	__render: function() {
		var widgets = TaskbarWidget.Widgets;
		this.__foreachWidget(function(widget) {
			var container = document.createElement("span");
			addCssClass(container, "taskbarWidget");
			
			this.__widgets[widget] = TaskbarWidget.Factory(widget, container);

			this.__container.appendChild(container);
			
			this.__widgets[widget].listen(this.__widgetMessage.bind(this));

		}.bind(this));
	},

	__foreachWidget: function(callback) {
		var widgets = TaskbarWidget.Widgets;
		for (var taskbarWidget in widgets) {
			var widget = widgets[taskbarWidget]
			if (this.__widgets[widget]) {
				widget = this.__widgets[widget];
			}
			callback(widget);
		}
	},

	__widgetMessage: function(widget) {
		this.__toolbar.widgetAction(widget.widget, widget.getValue());
	},
};

function TaskbarWidget(widget, container, caption) {
	this.callBaseMethod(TaskbarWidget);
	this.widget = widget;
	this._container = container;
	this._caption = caption;
	this._value = null;

	addCssClass(this._container, "widget" + this.widget);

	this._render();
}

TaskbarWidget.prototype = {
	notifySelecting: function(element, style, cache) { },

	notifySelectingComplete: function(cache) {
		var value = cache[this.widget];
		this.setValue(value);
	},

	setValue: function(val) {
		this._value = val;
		this._render();
	},

	getValue: function() {
		return this._value;
	},

	_render: function() {
		this._container.innerHTML = "";

		if (this._value) {
			var captionElem = this._getCaptionElem();
			var valueElem = this._getValueElem();
			if (captionElem) {
				this._container.appendChild(captionElem);
			}
			if (valueElem) {
				this._container.appendChild(valueElem);
			}
		} else {
			var noValueElem = this._getNoValueElem();
			if (noValueElem) {
				this._container.appendChild(noValueElem);
			}
		}
	},

	_getCaptionElem: function() {
		var elem = document.createElement("span");
		elem.innerHTML = this._caption + ":&nbsp;";
		return elem;
	},

	_getValueElem: function() {
		var elem = document.createElement("span");
		elem.innerHTML = this._value;
		return elem;
	},

	_getNoValueElem: function() {
	}
};

TaskbarWidget.Factory = function(widget, container) {
	var widgets = TaskbarWidget.Widgets;
	switch (widget) {
		case widgets.DummyWidget:
			return new TaskbarWidget(widget, container, "");
		case widgets.ForegroundColor:
			return new ForegroundColorDisplay(container);
		case widgets.BackgroundColor:
			return new BackgroundColorDisplay(container);
	}
}
TaskbarWidget.Widgets = {
	DummyWidget: 0,
	ForegroundColor: 1,
	BackgroundColor: 2
}
inherit(TaskbarWidget, Listener);

function TaskbarColorDisplay(widget, container, caption) {
	this.callBaseMethod(TaskbarColorDisplay, "", arguments);
}
TaskbarColorDisplay.prototype = {
}
inherit(TaskbarColorDisplay, TaskbarWidget);

function ForegroundColorDisplay(container) {
	this.callBaseMethod(ForegroundColorDisplay, "", [TaskbarWidget.Widgets.ForegroundColor, container, "fg"]);
}
ForegroundColorDisplay.prototype = {
	notifySelecting: function(element, style, cache) {
		var value = style.getPropertyValue("color");
		value = Color.parseString(value).nicestName();
		setFirstClearIfDifferent(value, cache, this.widget);
	},
	_getValueElem: function() {
		var val = this.callBaseMethod(ForegroundColorDisplay, "_getValueElem");
		val.style.color = this._value;
		var bg = Color.parseString(this._value);
		val.style.backgroundColor = bg.contrastColor();

		val.addEventListener("click", this._broadcast.bind(this, this));

		return val;
	}
};
inherit(ForegroundColorDisplay, TaskbarWidget);
function BackgroundColorDisplay(container) {
	this.callBaseMethod(BackgroundColorDisplay, "", [TaskbarWidget.Widgets.BackgroundColor, container, "bg"]);
}
BackgroundColorDisplay.prototype = {
	notifySelecting: function(element, style, cache) {
		var value = getComputedBackgroundColor("", element);
		value = Color.parseString(value).nicestName();
		setFirstClearIfDifferent(value, cache, this.widget);
	},
	_getValueElem: function() {
		var val = this.callBaseMethod(BackgroundColorDisplay, "_getValueElem");
		val.style.backgroundColor = this._value;
		var bg = Color.parseString(this._value);
		val.style.color = bg.contrastColor();

		val.addEventListener("click", this._broadcast.bind(this, this));

		return val;
	},
};
inherit(BackgroundColorDisplay, TaskbarWidget);
