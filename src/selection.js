

function Selection(container) {
	this.__container = container;

	this.__window = getWindow(container);
	this.__document = getDocument(container);
	
	this.range = getRangeSelected(this.__container);
	if (this.range) {
		this.__startContainer = this.range.startContainer;
		this.__endContainer = this.range.endContainer;
		this.__startContainerOffset = this.range.startOffset;
		this.__endContainerOffset = this.range.endOffset;
	}
}

Selection.placeCursorAtEnd = function (element) {
	Selection.select(element, element, 1, 1);
}

Selection.select = function(startContainer, endContainer, startOffset, endOffset) {
	if (startContainer && endContainer) {
		var win = getWindow(startContainer);
		var doc = getDocument(startContainer);

		var selection = win.getSelection();
		selection.removeAllRanges();

		var range = doc.createRange();
		range.setStart(startContainer, startOffset);
		range.setEnd(endContainer, endOffset);
		selection.addRange(range);
	}
}

Selection.prototype = {
	
	range: null,
	__container: null,
	__startContainer: null,
	__endContainer: null,
	__startContainerOffset: -1,
	__endContainerOffset: -1,
	__overallStartOffset: -1,
	__overallEndOffset: -1,


	traverseSelection: function(callback) {
		//<summary>traverses all leaf text nodes of a selection. Shouldn't necessarily assume order, but the order will be called the same each time</summary>
		//<param name="callback" type="Function">Callback to call on each selection. Is passed, 1) the node being considered, 2) the start offset (if it exists) where text in that node is selected, 3) the end offset (if it exists), 4) a count for how many nodes we have so far considered</param>
		if (!this.range) {
			return;
		}
		
		var sOffset = this.__startContainerOffset;
		var eOffset = this.__endContainerOffset;
		
		var sContainer = this.__startContainer;
		var eContainer = this.__endContainer;

		var i = 0;
		
		if (sContainer === eContainer) {
			callback(sContainer, sOffset, eOffset, i++);
		}
		else {
			var node = nextLeaf(this.__container, sContainer);
			callback(sContainer, sOffset, undefined, i++);
			while (node !== eContainer) {
				var oldNode = node;
				node = nextLeaf(this.__container, oldNode);
				callback(oldNode, undefined, undefined, i++);
			}
			callback(eContainer, undefined, eOffset, i++);
		}
	},

	restoreFromTextPosition: function() {

		var root = this.__container;
		var node = leftmostLeaf(root);
		
		var sOffset = this.__overallStartOffset;
		var eOffset = this.__overallEndOffset;

		var startContainer = null;
		var endContainer = null;

		var foundSOffset = -1;
		var foundEOffset = -1;

		var textSoFar = 0;

		while (node !== null) {
			var lastTextSoFar = textSoFar;
			textSoFar += this.__calculateLength(node);
			
			if (sOffset >= lastTextSoFar && sOffset <= textSoFar) {
				startContainer = node;
				foundSOffset = sOffset - lastTextSoFar;
			}

			if (eOffset >= lastTextSoFar && eOffset <= textSoFar) {
				endContainer = node;
				foundEOffset = eOffset - lastTextSoFar;
			}

			if (startContainer && endContainer) {
				break;
			}

			node = nextLeaf(root, node);
		}

		var selection = this.__window.getSelection();
		selection.removeAllRanges();
		if (startContainer && endContainer) {
			// success!
			var range = this.__document.createRange();
			range.setStart(startContainer, foundSOffset);
			range.setEnd(endContainer, foundEOffset);
			selection.addRange(range);
		}
	},

	analyzeSelectionTextPosition: function() {
		// goal: find position of the first node, count length along the way.
		var node = leftmostLeaf(this.__container);
		var len = 0;
		while (node !== null) {
			if (node === this.__startContainer) {
				break;
			}
			len += this.__calculateLength(node);
			node = nextLeaf(this.__containernode);
		}
		
		this.__overallStartOffset = len + this.__startContainerOffset;
		
		// now just find the offset of length, we just need to total length of the selection.
		// we have already calculated this if we traversed before.
		

		if (!this.range) {
			return {};
		}
		var startNode = this.__startContainer;
		var endNode = this.__endContainer;
		var sOffset = this.__startContainerOffset;
		var eOffset = this.__endContainerOffset;

		var textSoFar = 0;
		var root = this.__container;

		var overallSOffset = -1;
		var overallEOffset = -1;

		node = leftmostLeaf(root);
		while (node !== null) {
			if (node === startNode) {
				overallSOffset = textSoFar + sOffset;
			}
			
			if (node === endNode) {
				overallEOffset = textSoFar + eOffset;
			}
			
			textSoFar += this.__calculateLength(node);

			node = nextLeaf(root, node);
		}

		this.__overallStartOffset = overallSOffset;
		this.__overallEndOffset = overallEOffset;

	},

	isSameSelection: function(otherSelection) {
		return this.__overallStartOffset === otherSelection.__overallStartOffset;
	},

	addText: function(numChar) {
		this.__overallStartOffset += numChar;
		this.__overallEndOffset += numChar;
	},

	justCursor: function() {
		return this.__startContainer == this.__endContainer && this.__endContainerOffset === this.__startContainerOffset;
	},

	__calculateLength: function(node, sOffset, eOffset) {
		var text;

		var len;
		if (sOffset !== undefined && eOffset !== undefined) {
			len = eOffset - sOffset;
		}
		else {
			len = node.textContent.length;
			if (sOffset) {
				len -= sOffset;
			}
			if (eOffset) {
				len -= eOffset;
			}
		}

		return len
	},


	__id : function() { }

}
