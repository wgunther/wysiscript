function Texter() {}

Texter.__setText = function(parentNode, text, beforeChild) {
  // Reuse existing text node if available.
  if (parentNode.childNodes.length > 0) {
    var existing = logicalTextNode(
        beforeChild ? beforeChild.previousSibling
                    : parentNode.childNodes[parentNode.childNodes.length - 1]);
    if (existing) {
      existing.nodeValue = text;
      return;
    }
  }
  // No existing text node to reuse.
  var node = document.createTextNode(text);
  if (isUnderlined(parentNode)) {
    var nonsyntaxSpan = document.createElement("span");
    addCssClass(nonsyntaxSpan, "nonsyntax");
    nonsyntaxSpan.style.textDecoration = "underline";
    nonsyntaxSpan.appendChild(logicalTextNode);
    node = nonsyntaxSpan;
  }
  if (beforeChild) {
    parentNode.insertBefore(node, beforeChild);
  } else {
    parentNode.appendChild(node);
  }
};

// Syntax texting uses specifications (specs) that describe the appropriate
// contents of logical child text nodes before, after, and between logical
// child element nodes. A spec is an object with one or more of the following
// properties: `prefix`, `infix`, `suffix`, `nullary`, `label`, `use`, and
// `error`. The `error` property indicates a syntax error that is so
// significant that no meaningful syntax texting can be generated; if the
// `error` property is true, then no syntax texting is performed (the existing
// logical child text nodes are not changed). Otherwise, the spec is
// interpreted in one of three ways, as follows.
//
// List spec: A spec that has neither the `label` property nor the `use`
// property (see below). If one or more of the `prefix`, `infix`, `suffix`, and
// `nullary` properties are missing, they are set to their default values:
//   { prefix: "(", infix: ", ", suffix: ")", nullary: prefix + suffix }
// If the node has no element nodes as logical children, the `nullary` property
// will be used as a logical child text node. If the node has one or more
// element nodes as logical children, then the prefix will be used as a logical
// child text node before the first logical child element, the suffix will be
// used as a logical child text node after the last logical child element, and
// the infix will be used as many times as necessary as logical child text
// nodes separating the logical child elements.
//
// Label spec: A spec with the `label` property. This is shorthand for a list
// spec: { nullary: label, prefix: label + "(", infix: ", ", suffix: ")" }
//
// Use spec: A spec with the `use` property, and possibly the `prefix` and
// `suffix` properties (from function definition and/or variable assignment).
// The `use` property is a reference to a prototype function of the Texter
// class, which will be passed the node and the spec (as is, i.e., without
// setting the `prefix` and `suffix` properties to their default values if they
// are not set). That function is responsible for syntax-texting the node
// appropriately.

Texter.__cloneContext = function(context) {
  var clone = {};
  if (context.indent) clone.indent = context.indent;
  return clone;
};

Texter.__cloneSpec = function(spec) {
  if (!spec) return null;
  var clone = {};
  if (spec.prefix) clone.prefix = spec.prefix;
  if (spec.infix) clone.infix = spec.infix;
  if (spec.suffix) clone.suffix = spec.suffix;
  if (spec.nullary) clone.nullary = spec.nullary;
  if (spec.label) clone.label = spec.label;
  if (spec.use) clone.use = spec.use;
  if (spec.error) clone.error = spec.error;
  if (spec.context) clone.context = Texter.__cloneContext(spec.context);
  return clone;
};

Texter.__increaseIndent = function(spec) {
  if (!spec.context) spec.context = {};
  spec.context.indent = (spec.context.indent || 0) + 2;
};

Texter.__makeIndent = function(n) {
  return Array(n + 1).join(" ");
};

Texter.prototype.__applySpec = function(node, spec) {
  if (spec.error) return;
  if (spec.use) {
    return spec.use.bind(this, node, spec)();
  }
  var children = syntaxChildren(node);
  if (children.length === 0) {
    Texter.__setText(node, spec.nullary);
    return spec;
  }
  Texter.__setText(node, spec.prefix, children[0]);
  for (var i = 1; i < children.length; ++i) {
    var indent = (spec.context && spec.context.indent) || 0;
    var infix = spec.infix.replace(/\t/g, Texter.__makeIndent(indent));
    Texter.__setText(node, infix, children[i]);
  }
  Texter.__setText(node, spec.suffix);
  return spec;
};

Texter.prototype.__ifElse = function(node, spec) {
  var children = syntaxChildren(node);
  if (children.length === 0) {
    Texter.__setText(node, spec.prefix + "(if)" + spec.suffix);
    return spec;
  }
  if (children.length === 1) {
    Texter.__setText(node, spec.prefix + "(", children[0]);
    Texter.__setText(node, ")" + spec.suffix);
    return spec;
  }
  Texter.__setText(node, (spec.prefix || "(") + "if ", children[0]);
  var indent = Texter.__makeIndent((spec.context && spec.context.indent) || 0);
  Texter.__increaseIndent(spec);
  for (var i = 1; i < children.length; ++i) {
    if (i % 2 === 1) {
      Texter.__setText(node,
                       " then\n" + Texter.__makeIndent(spec.context.indent),
                       children[i]);
    } else if (i < children.length - 1) {
      Texter.__setText(node, "\n" + indent + "else if ", children[i]);
    } else {
      Texter.__setText(node,
                       "\n" + indent + "else\n" +
                           Texter.__makeIndent(spec.context.indent),
                       children[i]);
    }
  }
  Texter.__setText(node, spec.suffix || ")");
  return spec;
};

Texter.prototype.__teal = function(node, spec) {
  var children = syntaxChildren(node);
  if (children.length === 0) {
    Texter.__setText(node, spec.prefix + "eval until" + spec.suffix);
    return;
  }
  var indent = Texter.__makeIndent((spec.context && spec.context.indent) || 0);
  Texter.__increaseIndent(spec);
  Texter.__setText(node,
                   spec.prefix + "eval\n" +
                       Texter.__makeIndent(spec.context.indent),
                   children[0]);
  if (children.length === 1) {
    Texter.__setText(node, "\n" + indent + "until" + spec.suffix);
    return;
  }
  Texter.__setText(node, "\n" + indent + "until ", children[1]);
  for (var i = 2; i < children.length; ++i) {
    Texter.__setText(node, ", ", children[i]);
  }
  Texter.__setText(node, spec.suffix);
  return spec;
};

Texter.prototype.__navy = function(node, spec) {
  var children = syntaxChildren(node);
  if (children.length === 0) {
    Texter.__setText(node, spec.prefix + "(?[?])" + spec.suffix);
    return;
  }
  Texter.__setText(node, spec.prefix || "(", children[0]);
  if (children.length === 1) {
    Texter.__setText(node, "[?]" + (spec.suffix || ")"));
    return;
  }
  Texter.__setText(node, "[", children[1]);
  for (var i = 2; i < children.length; ++i) {
    Texter.__setText(node, "][", children[i]);
  }
  Texter.__setText(node, "]" + (spec.suffix || ")"));
  return spec;
};

Texter.prototype.__chartreuse = function(node, spec) {
  var children = syntaxChildren(node);
  if (children.length === 0) {
    Texter.__setText(node, spec.prefix + "(?.insert(? : ?))" + spec.suffix);
    return;
  }
  Texter.__setText(node, spec.prefix + "(", children[0]);
  if (children.length === 1) {
    Texter.__setText(node, ".insert(? : ?))" + spec.suffix);
    return;
  }
  Texter.__setText(node, ".insert(", children[1]);
  for (var i = 2; i < children.length - 1; ++i) {
    Texter.__setText(node, ", ", children[i]);
  }
  if (children.length === 2) {
    Texter.__setText(node, " : ?))" + spec.suffix);
    return;
  }
  Texter.__setText(node, " : ", children[children.length - 1]);
  Texter.__setText(node, "))" + spec.suffix);
  return spec;
};

Texter.prototype.__maroon = function(node, spec) {
  var children = syntaxChildren(node);
  if (children.length === 0) {
    Texter.__setText(node, spec.prefix + "(?.delete(?))" + spec.suffix);
    return;
  }
  Texter.__setText(node, spec.prefix + "(", children[0]);
  if (children.length === 1) {
    Texter.__setText(node, ".delete(?))" + spec.suffix);
    return;
  }
  Texter.__setText(node, ".delete(", children[1]);
  for (var i = 2; i < children.length; ++i) {
    Texter.__setText(node, ", ", children[i]);
  }
  Texter.__setText(node, "))" + spec.suffix);
  return spec;
};

Texter.builtinSpec = {};
// Function arguments
Texter.builtinSpec[Color.red] = { label: "$1" };
Texter.builtinSpec[Color.orange] = { label: "$2" };
Texter.builtinSpec[Color.yellow] = { label: "$3" };
Texter.builtinSpec[Color.green] = { label: "$4" };
Texter.builtinSpec[Color.blue] = { label: "$5" };
Texter.builtinSpec[Color.indigo] = { label: "$6" };
Texter.builtinSpec[Color.violet] = { label: "$7" };
// Control structures
Texter.builtinSpec[Color.honeydew] = { infix: ";\n\t", nullary: "0" };
Texter.builtinSpec[Color.rgb_1FE15E] = { use: Texter.prototype.__ifElse };
Texter.builtinSpec[Color.teal] = { use: Texter.prototype.__teal };
Texter.builtinSpec[Color.deepskyblue] = { label: "redshift" };
Texter.builtinSpec[Color.ghostwhite] = { prefix: "isUndefined(" };
Texter.builtinSpec[Color.rgb_5CA1A2] = { prefix: "isScalar(" };
Texter.builtinSpec[Color.fuchsia] = { prefix: "getFunction(" };
// Comparisons and Boolean operations
Texter.builtinSpec[Color.plum] = { infix: " == ", nullary: "1" };
Texter.builtinSpec[Color.rgb_1E55E2] = { infix: " < ", nullary: "1" };
Texter.builtinSpec[Color.rgb_B166E2] = { infix: " > ", nullary: "1" };
Texter.builtinSpec[Color.rgb_70661E] = { label: "!" };
Texter.builtinSpec[Color.rgb_A11] = { infix: " && ", nullary: "1" };
Texter.builtinSpec[Color.gold] = { infix: " || ", nullary: "1" };
// Math
Texter.builtinSpec[Color.rgb_ADD] = { infix: " + ", nullary: "0" };
Texter.builtinSpec[Color.rgb_D1FFE2] = { infix: " \u2212 ", nullary: "0" };
Texter.builtinSpec[Color.rgb_D07] = { infix: " * ", nullary: "1" };
Texter.builtinSpec[Color.rgb_D171DE] = { infix: " / ", nullary: "1" };
Texter.builtinSpec[Color.rgb_2E51D0] = { infix: " % ", nullary: "1/256" };
// TODO: Parentheses for powderblue?
Texter.builtinSpec[Color.powderblue] = { infix: " ** ", nullary: "1" };
Texter.builtinSpec[Color.rgb_106] = { prefix: "log(" };
Texter.builtinSpec[Color.rgb_AB5] = { prefix: "abs(" };  // Or |...| ?
Texter.builtinSpec[Color.rgb_F10002] = { prefix: "floor(" };
Texter.builtinSpec[Color.sienna] = { prefix: "sin(" };
Texter.builtinSpec[Color.rgb_C05] = { prefix: "cos(" };
Texter.builtinSpec[Color.tan] = { prefix: "tan(" };
Texter.builtinSpec[Color.moccasin] = { prefix: "asin(" };  // Or arcsin?
Texter.builtinSpec[Color.rgb_A2CC05] = { prefix: "acos(" };
Texter.builtinSpec[Color.rgb_A26] = { prefix: "atan2(" };
Texter.builtinSpec[Color.rgb_314159] = { label: "\u03c0" };
Texter.builtinSpec[Color.rgb_271828] = { label: "e" };
// Charts
Texter.builtinSpec[Color.coral] = { prefix: "[", suffix: "]" };
Texter.builtinSpec[Color.seashell] = { prefix: "isEmpty(" };
Texter.builtinSpec[Color.navy] = { use: Texter.prototype.__navy };
Texter.builtinSpec[Color.chartreuse] = { use: Texter.prototype.__chartreuse };
Texter.builtinSpec[Color.salmon] =
    { prefix: "reverseSort(keys(", suffix: "))" };
Texter.builtinSpec[Color.maroon] = { use: Texter.prototype.__maroon };
// Strings
Texter.builtinSpec[Color.rgb_DEC0DE] = { prefix: "parseFloat(" };
Texter.builtinSpec[Color.rgb_2EC0DE] = { prefix: "toString(" };
Texter.builtinSpec[Color.ivory] = { suffix: " in ['i', 'v', 'y'])" };
// I/O
Texter.builtinSpec[Color.rgb_6E7] = { prefix: "getchar(" };
Texter.builtinSpec[Color.rgb_FACADE] = { prefix: "print(" };
Texter.builtinSpec[Color.rgb_B00B00] = { prefix: "warn(" };  // ?
Texter.builtinSpec[Color.rgb_D1E] = { prefix: "die(" };

Texter.prototype.__textSubtree = function(node, parent_spec) {
  var italic = (node.style.fontStyle === "italic");
  var bold = (node.style.fontWeight === "bold");
  var underline = isUnderlined(node);
  var color = Color.parseString(node.style.color);
  var spec;
  if (bold && !underline) {  // Built-in
    spec = Texter.__cloneSpec(Texter.builtinSpec[color]);
  } else if (underline && !bold) {  // Numeric literal
    var num = 256 * color.red + color.green;
    if (num === 0) {
      spec = { label: "0" };
    } else {
      var denom = color.blue || 256;
      // Simplify fraction: Euclidean algorithm, then divide num, denom by gcd.
      var a = Math.max(num, denom), b = Math.min(num, denom);
      var r = a % b;  while (r > 0) { a = b;  b = r;  r = a % b; }
      num /= b;  denom /= b;
      spec = { label: num + (denom > 1 ? "/" + denom : "") };
    }
  } else if (!bold && !underline) {  // User-defined variable or function
    spec = { label: Texter.varName(color) };
  }
  if (spec) {
    // Patch in defaults.
    if (spec.use) {
      if (!spec.nullary) spec.nullary = "";
      if (!spec.prefix) spec.prefix = "";
      if (!spec.infix) spec.infix = "";
      if (!spec.suffix) spec.suffix = "";
    } else if (spec.label) {
      spec.nullary = (spec.prefix || "") + spec.label + (spec.suffix || "");
      spec.prefix = spec.label + "(";
      spec.infix = ", ";
      spec.suffix = ")";
    } else {
      if (!spec.prefix) spec.prefix = italic ? "" : "(";
      if (!spec.infix) spec.infix = ", ";
      if (!spec.suffix) spec.suffix = italic ? "" : ")";
      if (!spec.nullary) spec.nullary = spec.prefix + spec.suffix;
    }
    if (parent_spec) spec.context = Texter.__cloneContext(parent_spec.context);
    if (italic) {  // Function definition.
      Texter.__increaseIndent(spec);
      spec.prefix ="function {\n" + Texter.__makeIndent(spec.context.indent) +
                       spec.prefix;
      spec.suffix +=
          "\n" + Texter.__makeIndent(parent_spec.context.indent) + "}";
      spec.nullary = "function {\n" + Texter.__makeIndent(spec.context.indent) +
                         spec.nullary + "\n" +
                         Texter.__makeIndent(parent_spec.context.indent) + "}";
    }
    var backgroundColor = Color.parseString(node.style.backgroundColor,
                                            /*allowTransparent=*/true);
    if (backgroundColor.creationString !== "transparent") {  // Assignment.
      var varName = Texter.varName(backgroundColor);
      spec.prefix = "(" + varName + " := " + spec.prefix;
      spec.suffix += ")";
      spec.nullary = "(" + varName + " := " + spec.nullary + ")";
    }
    spec = this.__applySpec(node, spec);
  }
  var children = syntaxChildren(node);
  for (var i = 0; i < children.length; ++i) {
    this.__textSubtree(children[i], spec);
  }
};

Texter.prototype.text = function(root) {
  var children = syntaxChildren(root);
  for (var i = 0; i < children.length; ++i) {
    this.__textSubtree(children[i], {context: {indent: 0}});
    // Put newline after top-level nodes.
    if (i < children.length - 1) {
      var lastTextNode = rightmostLeaf(children[i]);
      lastTextNode.nodeValue += "\n";
    }
  }
  root.normalize();  // For good measure.
};

Texter.varName = function(color) {
  // TODO
  return color.nicestName();
};
