
var fontList = null;


function onLoad() {
	
	var toolbarDiv = document.getElementById("toolbar");
	var taskbarDiv = document.getElementById("taskbar");
	var menubarDiv = document.getElementById("menubar");

	// setup iframe
	var editorIFrame = document.getElementById("editorContainer");
	var iWindow = editorIFrame.contentWindow;
	var iDocument = iWindow.document;
	var editorDiv = iDocument.createElement("div");
	editorDiv.id = "editor";
	var link = document.createElement("link");
	link.href = "style.css";
	link.rel = "stylesheet";
	link.type = "text/css";
	iDocument.head.appendChild(link);
	iDocument.body.appendChild(editorDiv);

	fontList = new FontList();

	var colors = getCookie("colors");
	if (colors) {
		FontSelector.lastColors = colors.split("|").map(function(color) {
			return Color.parseString(color);
		});
	}
	
	// language components
	var linter = new Linter();
	var texter = new Texter();
	var interpreter = new Interpreter();
	
	// editor components
	var toolbar = new Toolbar(toolbarDiv);
	var taskbar = new Taskbar(taskbarDiv, toolbar);
	var editor = new Editor(editorDiv, toolbar, taskbar, linter, texter, interpreter);
	var menubar = new MenuBar(menubarDiv, editor, toolbar);
}

window.onload = onLoad;
