
function MenuBar(container, editor, toolbar) {
	MenuBar.initialize();

	this._container = container;
	this.editor = editor;
	this.toolbar = toolbar;
	
	this.__menus = [];

	this._fileSelect = null;
	this._render();
	this._hookBindings();

	this.hideAll();
}

MenuBar.prototype = {
	hideAll: function() {
		for (var menu in MenuBar.Menus) {
			var menuObj = this.__menus[menu];
			menuObj.hide();
		}
	},
	
	_hookBindings: function() {
		document.body.addEventListener("mouseup", this.hideAll.bind(this));
		this.editor.doc.body.addEventListener("mouseup", this.hideAll.bind(this));

		this._fileSelect.addEventListener("change", this._fileChanged.bind(this));
	},

	_fileChanged: function(e) {
		var file = e.target.files[0];
		if (!file) {
			return;
		}
		this._fileSelect.value = "";
		var reader = new FileReader();
		reader.onload = this._fileRead.bind(this);
		reader.readAsText(file);
	},

	_fileRead: function(e) {
		var contents = e.target.result;
		this.editor.display(contents);
	},

	_render: function() {
		for (var menu in MenuBar.Menus) {
			var span = document.createElement("span");
			addCssClass(span, "menu");

			this.__menus[menu] = new Menu(MenuBar.Menus[menu], menu, span, this);
			
			this.__menus[menu].listen(this._entrySelected.bind(this));

			this._container.appendChild(span);
		}

		this._fileSelect = document.createElement("input");
		this._fileSelect.type="file";
		this._fileSelect.style.position = "fixed";
		this._fileSelect.style.top = "-1000px";
		this._container.appendChild(this._fileSelect);

	},

	_entrySelected: function(entry) {
		entries = MenuBar.Entries;
		switch(entry.caption) {
			case entries.New:
				this.editor.makeNew();
				break;
			case entries.Open:
				var e = document.createEvent("MouseEvents");
				e.initEvent("click", true, false);
				this._fileSelect.dispatchEvent(e);
				break;
			case entries.Reference:
			case entries.About:
			case entries.Save:
			case entries.Exit:
				var e = document.createEvent("MouseEvents");
				e.initEvent("click", true, false);
				entry.span.dispatchEvent(e); // HACK
				break;
			case entries.AutoSyntaxText:
				Settings.autotext = !Settings.autotext;
				break
			case entries.AutoLint:
				Settings.autolint = !Settings.autolint;
				break;
			case entries.SyntaxText:
				this.editor.text(true);
				break
			case entries.Lint:
				this.editor.lint(true);
				break;
			case entries.Run:
				this.editor.run(true);
				break;
			case entries.Divider:
				break;
			case entries.Undo:
				this.editor.undo();
				break;
			case entries.Redo:
				this.editor.redo();
				break;
			default:
				if (entry.menu.kind === MenuBar.Menus.Examples) {
					var example = this._getExample(entry.caption);
					if (example) {
						this.editor.display(example.contents);
					}
					break;
				}
				alert("Not implemented");
		}
	},

	_getExample: function(caption) {
		var examples = Example.Examples;
		for (var i in examples) {
			var example = examples[i];
			if (example.name === caption) {
				return example;
			}
		}
	},
}

MenuBar.Entries = {
	New: "New",
	Open: "Open",
	Save: "Save",
	Exit: "Exit",
	AutoSyntaxText: "Automatic Syntax Texting",
	AutoLint: "Automatic Linting",
	SyntaxText: "Syntax Text",
	Lint: "Lint",
	Reference: "Language Reference",
	About: "About",
	Run: "Run",
	Divider: "Divider",
	Undo: "Undo",
	Redo: "Redo",
}

// add examples
MenuBar.initialize = function() {
	if (MenuBar.initialized) {
		return;
	}
	var menus = MenuBar.Menus;
	var entries = MenuBar.Entries;
	var map = MenuBar.MenuBarEntryMap;

	// examples
	map[menus.Examples] =[]
	var examples = Example.Examples;
	for (var i in Example.Examples) {
		var example = Example.Examples[i];
		MenuBar.Entries["Example" + i] = example.name;
		map[menus.Examples].push(example.name);
	}
	MenuBar.initialized = true;


	// file
	map[menus.File] = [
		entries.New,
		entries.Open,
		entries.Save,
		entries.Exit,
	];
	
	// edit
	map[menus.Edit] = [
		entries.Undo,
		entries.Redo,
	];

	//action
	map[menus.Actions] = [
		entries.Run,
		entries.SyntaxText,
		entries.Divider,
		entries.AutoSyntaxText,
	];


	//help
	map[menus.Help] =[
		entries.Reference,
		entries.About
	]
}

MenuBar.initialized = false;

MenuBar.Menus = {
	File: 1,
	Edit: 2,
	Actions: 3,
	Examples: 5,
	Help: 6,
}

MenuBar.MenuBarEntryMap = [];

function Menu(kind, caption, container, menubar) {
	this.callBaseMethod(Menu);
	this._container = container;
	this.menubar = menubar;
	this.caption = caption;
	this.kind = kind;

	this.__open = null;

	this.entries = [];

	this._render();
	this._hookBindings();
}

Menu.prototype = {
	_render: function() {
		this._ui = document.createElement("button");
		this._ui.innerHTML = this.caption;
		this._container.appendChild(this._ui);
	},

	_hookBindings: function() {
		this._ui.addEventListener("click", this.openMenu.bind(this));
	},

	hide: function() {
		if (this.__open) {
			this.__open.innerHTML = "";
			if (this.__open.parentNode) {
				this.__open.parentNode.removeChild(this.__open);
			}
			this.__open = "";
		}
	},

	openMenu: function() {
		var entries = MenuBar.MenuBarEntryMap[this.kind];
		
		var div = document.createElement("div");
		this.__open = div;
		addCssClass(div, "openMenu");

		for (var i = 0; i < entries.length; ++i) {
			var entryName = entries[i];

			var span = document.createElement("span");
			

			var entry = new MenuEntry(span, this, this.menubar, entryName);

			addCssClass(span, "menuEntry");
			if (entry.selectable) {
				addCssClass(span, "selectableMenuEntry");
			}
			
			entry.listen(this._entrySelected.bind(this));

			div.appendChild(span);
		}
	
		var x = this._container.offsetLeft;
		var y = this._container.offsetTop + this._container.clientHeight;

		document.body.appendChild(div);
		div.style.position = "absolute";
		div.style.top = y + "px";
		div.style.left = x + "px";
	},

	_entrySelected: function(entry) {
		this._broadcast(entry);
	}
}
inherit(Menu, Listener);

function MenuEntry(container, menu, menubar, caption) {
	this.callBaseMethod(MenuEntry);
	this._container = container;
	this.menu = menu;
	this.menubar = menubar;
	this.caption = caption;
	this.selectable = true;

	this._render();
	this._hookBindings();
}

MenuEntry.prototype = { 
	_render: function() {
		var span;
		var entries = MenuBar.Entries;
		var addText = true;
		switch (this.caption) {
			case entries.Save:
				span = document.createElement("a");
				var blob = this.menubar.editor.blob();
				span.download = "a.wysi";
				span.draggable = true;
				span.href = window.URL.createObjectURL(blob);
				break;
			case entries.About:
				span = document.createElement("a");
				span.href = "sigbovik.html";
				span.target = "_blank";
				break;
			case entries.Reference:
				span = document.createElement("a");
				span.href = "ref.html";
				span.target = "_blank";
				break;
			case entries.Exit:
				span = document.createElement("a");
				span.href = "index.html";
				break;
			case entries.AutoLint:
				span = document.createElement("span");
				span.innerHTML = this._makeCheckbox(Settings.autolint);
				break;
			case entries.AutoSyntaxText:
				span = document.createElement("span");
				span.innerHTML = this._makeCheckbox(Settings.autotext);
				break;
			case entries.Divider:
				span = document.createElement("hr");
				addText = false;
				this.selectable = false;
				break;
			default:
				span = document.createElement("span");
		}
		if (addText) {
			span.innerHTML = span.innerHTML + this.caption;
		}
		this.span = span;
		this._container.appendChild(span);
	},
	
	_makeCheckbox: function(checked) {
		var html;
		html = "<span class='checkbox'>";
		if (checked) { html += "\u2611"; }
		else { html += "\u2610"; }
		html += "</span>";
		return html;
	},

	_hookBindings: function() {
		this._container.addEventListener("mousedown", this.__selected.bind(this));
	},

	__selected: function() {
		this._broadcast(this);
	}
}

inherit(MenuEntry, Listener);
