
function ColorSelector(rows, cols, color, lastColors, caption) {
	this.callBaseMethod(ColorSelector);
	this.__rows = rows;
	this.__cols = cols;

	this.__caption = caption;

	this.__lastColors = lastColors || [];
	this.selectedColor = color || Color.black;

}

ColorSelector.prototype = {
	
	show: function(container) {
		this.hide();
		this.__render(container);
	},

	hide: function() {
		if (this.__container) {
			this.__container.innerHTML = "";
			if (this.__container.parentNode) {
				this.__container.parentNode.removeChild(this.__container);
			}
			this.__container = "";
		}
	},

	__render: function(container) {
		
		this.__container = container;

		addCssClass(container, "colorSelector");

		if (this.__caption) {
			var captionElem = document.createElement("div");
			addCssClass(captionElem, "colorSelectorCaption");
			captionElem.innerHTML = this.__caption;
			this.__container.appendChild(captionElem);
		}

		var textField = document.createElement("input");
		textField.placeholder = "Type color here...";
		container.appendChild(textField);
		addCssClass(textField, "colorSelectorField");

		this.__setupColorField(textField);

		var recent = document.createElement("div");
		container.appendChild(recent);
		addCssClass(recent, "recentColors");

		for (var i = 0; i < this.__rows; ++i) {
			var row = document.createElement("div");
			addCssClass(row, "recentColorRow");
			recent.appendChild(row);

			for (var j = 0; j < this.__cols; ++j) {
				var entryNum = i * this.__cols + j;

				
				this.__setupColorEntry(entryNum, row);

			}
		}
	},

	__setupColorField: function(field) {
		field.addEventListener("keydown", this.__inputKeydown.bind(this, field));
		field.addEventListener("blur", this.__onBlur.bind(this));
		field.focus();
	},

	__onBlur: function() {
		this.hide();
	},

	__setupColorEntry: function(entryNum, row) {

		var entry = document.createElement("button");
		row.appendChild(entry);
		addCssClass(entry, "recentColorEntry");

		entry.addEventListener("mousedown", this.__entryClicked.bind(this, entryNum));

		var color = this.__lastColors[entryNum] || Color.white;

		entry.style.backgroundColor = color;
		entry.title = color.nicestName();
	},

	__entryClicked: function(entryNum) {
		this.colorSelected(this.__lastColors[entryNum] || Color.white);
		this.hide();
	},

	__inputKeydown: function(field, e) {
		var key = e.key;
		this._hideError(field);
		if (key === "Enter") {
			var color = Color.parseString(field.value);
			if (color) {
				this.colorSelected(color);
				this.hide();
			}
			else {
				this._showError(field);
			}
		}
	},

	_showError: function(field) {
		this.__error = true;
		field.style.backgroundColor = "pink";
	},

	_hideError: function(field) {
		if (this.__error) {
			this.__error = false;
			field.style.backgroundColor = "white";
		}
	},

	colorSelected: function(color) {
		this.selectedColor = color;
		var index = this.__lastColors.indexOf(color);
		if (index >= 0) {
			this.__lastColors.splice(index,1);
		}
		this.__lastColors.unshift(color);
		
		var forCookie = this.__lastColors.join("|");
		setCookie("colors", forCookie);

		this._broadcast(color);
	}

};

inherit(ColorSelector, Listener);
