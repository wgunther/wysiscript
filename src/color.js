// Transparent is represented by
//   { red: 255, green: 255, blue: 255, creationString: "transparent" },
// regardless of the actual creation string.
function Color(red, green, blue, creationString) {
  this.red = red;
  this.green = green;
  this.blue = blue;
  this.creationString = creationString ||
                         "rgb(" + red + ", " + green + ", " + blue + ")";
}

// NOTE: We are using a bad kludge to test whether a string is a CSS color
// name. The CSS colors are all listed below as properties of the Color class.
// This provides a nice short way to refer to them (Color.aliceblue, etc.) in
// the interpreter and syntax-texter code, which benefits from conciseness.
// We test for them here by converting the string to lowercase and seeing
// whether (a) the string is all letters and (b) the Color class has a property
// by that name. But this means that the Color class had better not have any
// other properties whose names are all lowercase letters!
Color.parseString = function(string, allowTransparent) {
  var lc = string.toLowerCase();
  if (/^[a-z]+$/.test(lc) && Color.hasOwnProperty(lc)) return Color[lc];
  var span = document.createElement("span");
  span.style.backgroundColor = string;
  var resolved = span.style.backgroundColor;
  if (resolved) {
    var rgb = resolved.substring(resolved.indexOf('(') + 1,
                                 resolved.indexOf(')'))
                      .split(/,\s*/);
    // Handle both rgb(x, x, x) and rgba(x, x, x, x).
    if (lc === "transparent" || (rgb.length === 4 && rgb[3] === 0)) {
      if (allowTransparent) return new Color(255, 255, 255, "transparent");
      throw "Syntax error: color '" + string + "' not allowed here";
    }
    if (rgb.length === 3 || rgb.length === 4) {
      return new Color(+rgb[0], +rgb[1], +rgb[2], string);
    }
  }
  // Easter egg: Support character and floating-point literals and fractions.
  if ("'\"".indexOf(string.charAt(0)) >= 0 && string.length >= 2) {  // Char.
    return Color.fromNumber(string.codePointAt(1));
  }
  if (/^([0-9]+(\.[0-9]*)?|\.[0-9]+)$/.test(string)) {  // Floating-point.
    return Color.fromNumber(parseFloat(string));
  }
  if (/^[0-9]+\/[0-9]*[1-9][0-9]*$/.test(string)) {  // Fraction.
    var frac = string.split("/");
    return Color.fromNumber(frac[0] / frac[1]);
  }
  return null;
};

Color.prototype.equals = function(that) {
  if (typeof that === "string") {
    that = Color.parseString(that);
  }
  if (!that) return false;
  if (this.creationString === "transparent") {
    return that.creationString === "transparent";
  }
  return this.red === that.red && this.green === that.green &&
         this.blue === that.blue;
};

Color.prototype.toString = function() {
  if (this.creationString === "transparent") return "transparent";
  return "rgb(" + this.red + ", " + this.green + ", " + this.blue + ")";
};

Color.prototype.toNumber = function() {
  return (256 * this.red + this.green) / (this.blue || 256);
};

// Returns a color for the nearest representable number to x. If there are two
// representable numbers equally close to x, one of them is chosen at random;
// likewise if the nearest representable number has multiple representations as
// a color. Variety is the spice of life.
Color.fromNumber = function(x) {
  var randInt = function(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
  };
  if (x < 1/512 || (x === 1/512 && randInt(0, 1) === 0)) {
    // Nearest representable number is 0.
    return new Color(0, 0, randInt(0, 255));
  }
  if (x === 1/512) { return new Color(0, 1, 0); }
  if (x > 65534.5 || (x === 65534.5 && randInt(0, 1) === 1)) {
    // Nearest representable number is 65535.
    return new Color(255, 255, 1);
  }
  if (x === 65534.5) { return new Color(255, 254, 1); }
  // Best rational approximation via truncated continued fractions.
  var compute = function(cfrac) {
    if (cfrac.length === 0) return [0, 1];
    var num = cfrac[cfrac.length - 1], denom = 1;
    for (var i = cfrac.length - 2; i >= 0; --i) {
      var t = num;  num = denom;  denom = t;
      num += cfrac[i] * denom;
    }
    return [num, denom];
  };
  var cfrac = [Math.floor(x)];        // Current continued fraction.
  var f = x - Math.floor(x);          // Current fractional component.
  var bestFrac = [Math.floor(x), 1];  // Best num, denom so far.
  var bestDist = f;                   // Smallest distance achieved so far.
  hunt: while (f > 1/1024) {
    f = 1/f;
    var term = Math.floor(f);
    f -= term;
    cfrac.push(term);
    var frac = compute(cfrac);
    if (frac[0] > 65535 || frac[1] > 256) {
      var reduceSuccessful = false;
      for (var r = term - 1; r >= term / 2; --r) {
        cfrac[cfrac.length - 1] = r;
        frac = compute(cfrac);
        if (frac[0] <= 65535 && frac[1] <= 256) {
          reduceSuccessful = true;
          break;
        }
      }
      if (!reduceSuccessful) break hunt;
    }
    var val = frac[0] / frac[1];
    var dist = Math.abs(x - val);
    if (dist < bestDist || (dist == bestDist && randInt(0, 1) === 1)) {
      bestFrac = frac.slice();
      bestDist = dist;
    }
    if (cfrac[cfrac.length - 1] < term) break;  // Reduction was necessary.
  }
  var multiplier =
      randInt(1, Math.floor(Math.min(65535 / bestFrac[0], 256 / bestFrac[1])));
  bestFrac[0] *= multiplier;  bestFrac[1] *= multiplier;
  return new Color(Math.floor(bestFrac[0] / 256), bestFrac[0] % 256,
                   bestFrac[1] === 256 ? 0 : bestFrac[1]);
};

// "Extended color keywords" from CSS Color Module Level 3 (editor's draft
// 7 July 2014): https://drafts.csswg.org/css-color-3/#svg-color
Color.aliceblue = new Color(240, 248, 255, "aliceblue");
Color.antiquewhite = new Color(250, 235, 215, "antiquewhite");
Color.aqua = new Color(0, 255, 255, "aqua");
Color.aquamarine = new Color(127, 255, 212, "aquamarine");
Color.azure = new Color(240, 255, 255, "azure");
Color.beige = new Color(245, 245, 220, "beige");
Color.bisque = new Color(255, 228, 196, "bisque");
Color.black = new Color(0, 0, 0, "black");
Color.blanchedalmond = new Color(255, 235, 205, "blanchedalmond");
Color.blue = new Color(0, 0, 255, "blue");
Color.blueviolet = new Color(138, 43, 226, "blueviolet");
Color.brown = new Color(165, 42, 42, "brown");
Color.burlywood = new Color(222, 184, 135, "burlywood");
Color.cadetblue = new Color(95, 158, 160, "cadetblue");
Color.chartreuse = new Color(127, 255, 0, "chartreuse");
Color.chocolate = new Color(210, 105, 30, "chocolate");
Color.coral = new Color(255, 127, 80, "coral");
Color.cornflowerblue = new Color(100, 149, 237, "cornflowerblue");
Color.cornsilk = new Color(255, 248, 220, "cornsilk");
Color.crimson = new Color(220, 20, 60, "crimson");
Color.cyan = new Color(0, 255, 255, "cyan");
Color.darkblue = new Color(0, 0, 139, "darkblue");
Color.darkcyan = new Color(0, 139, 139, "darkcyan");
Color.darkgoldenrod = new Color(184, 134, 11, "darkgoldenrod");
Color.darkgray = new Color(169, 169, 169, "darkgray");
Color.darkgreen = new Color(0, 100, 0, "darkgreen");
Color.darkgrey = new Color(169, 169, 169, "darkgrey");
Color.darkkhaki = new Color(189, 183, 107, "darkkhaki");
Color.darkmagenta = new Color(139, 0, 139, "darkmagenta");
Color.darkolivegreen = new Color(85, 107, 47, "darkolivegreen");
Color.darkorange = new Color(255, 140, 0, "darkorange");
Color.darkorchid = new Color(153, 50, 204, "darkorchid");
Color.darkred = new Color(139, 0, 0, "darkred");
Color.darksalmon = new Color(233, 150, 122, "darksalmon");
Color.darkseagreen = new Color(143, 188, 143, "darkseagreen");
Color.darkslateblue = new Color(72, 61, 139, "darkslateblue");
Color.darkslategray = new Color(47, 79, 79, "darkslategray");
Color.darkslategrey = new Color(47, 79, 79, "darkslategrey");
Color.darkturquoise = new Color(0, 206, 209, "darkturquoise");
Color.darkviolet = new Color(148, 0, 211, "darkviolet");
Color.deeppink = new Color(255, 20, 147, "deeppink");
Color.deepskyblue = new Color(0, 191, 255, "deepskyblue");
Color.dimgray = new Color(105, 105, 105, "dimgray");
Color.dimgrey = new Color(105, 105, 105, "dimgrey");
Color.dodgerblue = new Color(30, 144, 255, "dodgerblue");
Color.firebrick = new Color(178, 34, 34, "firebrick");
Color.floralwhite = new Color(255, 250, 240, "floralwhite");
Color.forestgreen = new Color(34, 139, 34, "forestgreen");
Color.fuchsia = new Color(255, 0, 255, "fuchsia");
Color.gainsboro = new Color(220, 220, 220, "gainsboro");
Color.ghostwhite = new Color(248, 248, 255, "ghostwhite");
Color.gold = new Color(255, 215, 0, "gold");
Color.goldenrod = new Color(218, 165, 32, "goldenrod");
Color.gray = new Color(128, 128, 128, "gray");
Color.green = new Color(0, 128, 0, "green");
Color.greenyellow = new Color(173, 255, 47, "greenyellow");
Color.grey = new Color(128, 128, 128, "grey");
Color.honeydew = new Color(240, 255, 240, "honeydew");
Color.hotpink = new Color(255, 105, 180, "hotpink");
Color.indianred = new Color(205, 92, 92, "indianred");
Color.indigo = new Color(75, 0, 130, "indigo");
Color.ivory = new Color(255, 255, 240, "ivory");
Color.khaki = new Color(240, 230, 140, "khaki");
Color.lavender = new Color(230, 230, 250, "lavender");
Color.lavenderblush = new Color(255, 240, 245, "lavenderblush");
Color.lawngreen = new Color(124, 252, 0, "lawngreen");
Color.lemonchiffon = new Color(255, 250, 205, "lemonchiffon");
Color.lightblue = new Color(173, 216, 230, "lightblue");
Color.lightcoral = new Color(240, 128, 128, "lightcoral");
Color.lightcyan = new Color(224, 255, 255, "lightcyan");
Color.lightgoldenrodyellow = new Color(250, 250, 210, "lightgoldenrodyellow");
Color.lightgray = new Color(211, 211, 211, "lightgray");
Color.lightgreen = new Color(144, 238, 144, "lightgreen");
Color.lightgrey = new Color(211, 211, 211, "lightgrey");
Color.lightpink = new Color(255, 182, 193, "lightpink");
Color.lightsalmon = new Color(255, 160, 122, "lightsalmon");
Color.lightseagreen = new Color(32, 178, 170, "lightseagreen");
Color.lightskyblue = new Color(135, 206, 250, "lightskyblue");
Color.lightslategray = new Color(119, 136, 153, "lightslategray");
Color.lightslategrey = new Color(119, 136, 153, "lightslategrey");
Color.lightsteelblue = new Color(176, 196, 222, "lightsteelblue");
Color.lightyellow = new Color(255, 255, 224, "lightyellow");
Color.lime = new Color(0, 255, 0, "lime");
Color.limegreen = new Color(50, 205, 50, "limegreen");
Color.linen = new Color(250, 240, 230, "linen");
Color.magenta = new Color(255, 0, 255, "magenta");
Color.maroon = new Color(128, 0, 0, "maroon");
Color.mediumaquamarine = new Color(102, 205, 170, "mediumaquamarine");
Color.mediumblue = new Color(0, 0, 205, "mediumblue");
Color.mediumorchid = new Color(186, 85, 211, "mediumorchid");
Color.mediumpurple = new Color(147, 112, 219, "mediumpurple");
Color.mediumseagreen = new Color(60, 179, 113, "mediumseagreen");
Color.mediumslateblue = new Color(123, 104, 238, "mediumslateblue");
Color.mediumspringgreen = new Color(0, 250, 154, "mediumspringgreen");
Color.mediumturquoise = new Color(72, 209, 204, "mediumturquoise");
Color.mediumvioletred = new Color(199, 21, 133, "mediumvioletred");
Color.midnightblue = new Color(25, 25, 112, "midnightblue");
Color.mintcream = new Color(245, 255, 250, "mintcream");
Color.mistyrose = new Color(255, 228, 225, "mistyrose");
Color.moccasin = new Color(255, 228, 181, "moccasin");
Color.navajowhite = new Color(255, 222, 173, "navajowhite");
Color.navy = new Color(0, 0, 128, "navy");
Color.oldlace = new Color(253, 245, 230, "oldlace");
Color.olive = new Color(128, 128, 0, "olive");
Color.olivedrab = new Color(107, 142, 35, "olivedrab");
Color.orange = new Color(255, 165, 0, "orange");
Color.orangered = new Color(255, 69, 0, "orangered");
Color.orchid = new Color(218, 112, 214, "orchid");
Color.palegoldenrod = new Color(238, 232, 170, "palegoldenrod");
Color.palegreen = new Color(152, 251, 152, "palegreen");
Color.paleturquoise = new Color(175, 238, 238, "paleturquoise");
Color.palevioletred = new Color(219, 112, 147, "palevioletred");
Color.papayawhip = new Color(255, 239, 213, "papayawhip");
Color.peachpuff = new Color(255, 218, 185, "peachpuff");
Color.peru = new Color(205, 133, 63, "peru");
Color.pink = new Color(255, 192, 203, "pink");
Color.plum = new Color(221, 160, 221, "plum");
Color.powderblue = new Color(176, 224, 230, "powderblue");
Color.purple = new Color(128, 0, 128, "purple");
Color.red = new Color(255, 0, 0, "red");
Color.rosybrown = new Color(188, 143, 143, "rosybrown");
Color.royalblue = new Color(65, 105, 225, "royalblue");
Color.saddlebrown = new Color(139, 69, 19, "saddlebrown");
Color.salmon = new Color(250, 128, 114, "salmon");
Color.sandybrown = new Color(244, 164, 96, "sandybrown");
Color.seagreen = new Color(46, 139, 87, "seagreen");
Color.seashell = new Color(255, 245, 238, "seashell");
Color.sienna = new Color(160, 82, 45, "sienna");
Color.silver = new Color(192, 192, 192, "silver");
Color.skyblue = new Color(135, 206, 235, "skyblue");
Color.slateblue = new Color(106, 90, 205, "slateblue");
Color.slategray = new Color(112, 128, 144, "slategray");
Color.slategrey = new Color(112, 128, 144, "slategrey");
Color.snow = new Color(255, 250, 250, "snow");
Color.springgreen = new Color(0, 255, 127, "springgreen");
Color.steelblue = new Color(70, 130, 180, "steelblue");
Color.tan = new Color(210, 180, 140, "tan");
Color.teal = new Color(0, 128, 128, "teal");
Color.thistle = new Color(216, 191, 216, "thistle");
Color.tomato = new Color(255, 99, 71, "tomato");
Color.turquoise = new Color(64, 224, 208, "turquoise");
Color.violet = new Color(238, 130, 238, "violet");
Color.wheat = new Color(245, 222, 179, "wheat");
Color.white = new Color(255, 255, 255, "white");
Color.whitesmoke = new Color(245, 245, 245, "whitesmoke");
Color.yellow = new Color(255, 255, 0, "yellow");
Color.yellowgreen = new Color(154, 205, 50, "yellowgreen");

// Colors for WysiScript built-ins expressed as #RGB or #RRGGBB.
Color.rgb_106 = Color.parseString("#106");
Color.rgb_6E7 = Color.parseString("#6E7");
Color.rgb_A11 = Color.parseString("#A11");
Color.rgb_A26 = Color.parseString("#A26");
Color.rgb_AB5 = Color.parseString("#AB5"),
Color.rgb_ADD = Color.parseString("#ADD");
Color.rgb_C05 = Color.parseString("#C05");
Color.rgb_D07 = Color.parseString("#D07");
Color.rgb_D1E = Color.parseString("#D1E");
Color.rgb_1E55E2 = Color.parseString("#1E55E2");
Color.rgb_1FE15E = Color.parseString("#1FE15E");
Color.rgb_2E51D0 = Color.parseString("#2E51D0");
Color.rgb_2EC0DE = Color.parseString("#2EC0DE");
Color.rgb_271828 = Color.parseString("#271828");
Color.rgb_314159 = Color.parseString("#314159");
Color.rgb_5CA1A2 = Color.parseString("#5CA1A2");
Color.rgb_70661E = Color.parseString("#70661E");
Color.rgb_A2CC05 = Color.parseString("#A2CC05");
Color.rgb_B00B00 = Color.parseString("#B00B00");
Color.rgb_B166E2 = Color.parseString("#B166E2");
Color.rgb_D171DE = Color.parseString("#D171DE");
Color.rgb_D1FFE2 = Color.parseString("#D1FFE2");
Color.rgb_DEC0DE = Color.parseString("#DEC0DE");
Color.rgb_F10002 = Color.parseString("#F10002");
Color.rgb_FACADE = Color.parseString("#FACADE");

Color.builtinFunctions = [
  // Control structures
  Color.honeydew, Color.rgb_1FE15E, Color.teal, Color.deepskyblue,
  Color.ghostwhite, Color.rgb_5CA1A2, Color.fuchsia,
  // Comparisons and Boolean operations
  Color.plum, Color.rgb_1E55E2, Color.rgb_B166E2, Color.rgb_70661E,
  Color.rgb_A11, Color.gold,
  // Math
  Color.rgb_ADD, Color.rgb_D1FFE2, Color.rgb_D07, Color.rgb_D171DE,
  Color.rgb_2E51D0, Color.powderblue, Color.rgb_106, Color.rgb_AB5,
  Color.rgb_F10002, Color.sienna, Color.rgb_C05, Color.tan, Color.moccasin,
  Color.rgb_A2CC05, Color.rgb_A26, Color.rgb_314159, Color.rgb_271828,
  // Charts
  Color.coral, Color.seashell, Color.navy, Color.chartreuse, Color.salmon,
  Color.maroon,
  // Strings
  Color.rgb_DEC0DE, Color.rgb_2EC0DE, Color.ivory,
  // I/O
  Color.rgb_6E7, Color.rgb_FACADE, Color.rgb_B00B00, Color.rgb_D1E,
];

// Build a map from colors back to their CSS names.
Color.cssName = {};
for (var c in Color) {
  if (/^[a-z]+$/.test(c) && Color.hasOwnProperty(c)) {
    Color.cssName[Color[c]] = c;
  }
}
// Some colors have multiple CSS names; be explicit about which one wins.
Color.cssName[Color.aqua] = "aqua";  // Not cyan.
Color.cssName[Color.darkgray] = "darkgray";  // Not darkgrey.
Color.cssName[Color.darkslategray] = "darkslategray";  // Not darkslategrey.
Color.cssName[Color.dimgray] = "dimgray";  // Not dimgrey.
Color.cssName[Color.fuchsia] = "fuchsia";  // Not magenta.
Color.cssName[Color.gray] = "gray";  // Not grey.
Color.cssName[Color.lightgray] = "lightgray";  // Not lightgrey.
Color.cssName[Color.lightslategray] = "lightslategray";  // Not lightslategrey.
Color.cssName[Color.slategray] = "slategray";  // Not slategrey.

Color.__hexify = function(octet) {
  var hex = octet.toString(16).toUpperCase();
  if (hex.length == 1) hex = "0" + hex;
  return hex;
};

// Returns a CSS color name if one exists for this color, or else a three-digit
// RGB hex code if possible, or else a six-digit RGB hex code.
Color.prototype.nicestName = function() {
  var cssName = Color.cssName[this];
  if (cssName) return cssName;
  var rgbHex = Color.__hexify(this.red) + Color.__hexify(this.green) +
               Color.__hexify(this.blue);
  if (/^(.)\1(.)\2(.)\3$/.test(rgbHex)) {
    rgbHex = rgbHex.substr(0, 1) + rgbHex.substr(2, 1) + rgbHex.substr(4, 1);
  }
  return "#" + rgbHex;
};

Color.prototype.contrastColor = function() {
	if (this.luma() >= 165) {
		return Color.black;
	}
	else {
		return Color.white;
	}
};

Color.prototype.luma = function() {
	return this.red * .2126 + this.green * .7152 + this.blue * .0722;
};
