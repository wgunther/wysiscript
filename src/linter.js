function Linter() {}

Linter.prototype.__getFontSize = function(style) {
  return parseFloat(style.getPropertyValue("font-size"));  // In px.
};

Linter.prototype.__getFontFamily = function(style) {
	return getFontFamily(style);
};

Linter.prototype.__getItalic = function(style) {
	return isItalic(style);
};

Linter.prototype.__getBold = function(style) {
	return isBold(style);
};

// Underline is complicated because it isn't inherited by children. Example:
//   <span style="text-decoration:underline"><span>X</span></span>
// The computed style of the text node here says text-decoration:none, because
// that node *isn't* underlined (it's the parent node that's underlined). But
// we want to consider the text node to be underlined because that is the style
// that was set by the nearest ancestor that explicitly set text-decoration. So
// we go up the tree until we find a node that sets text-decoration, and use
// the value there. If we get all the way to the root, return false.
//
// NOTE: The return value here might be false even if the element actually
// appears to be underlined in the browser. This is because descendants can't
// turn off underlining from their ancestors. Example:
//   <span style="text-decoration:underline">
//     <span style="text-decoration:none">X</span>
//   </span>
// The text node here will appear underlined because the outer span is, and
// there's nothing the inner span can do about that. However, we want the
// editor to be able to "turn off" underlining with this kind of trick, so to
// support that this function will return false if it is called on the inner
// span. Linting will fix it up so that the underlining is actually turned off.
Linter.prototype.__getIntendedUnderline = function(root, elem) {
  var props = ['textDecoration', 'textDecorationLine'];
  while (elem !== root) {
    if (elem.tagName && elem.tagName.toLowerCase() === "u") return true;
    for (var i = 0; i < props.length; ++i) {
      var prop = elem.style[props[i]];
      if (prop) {
        if (prop.search("underline") >= 0) return true;
        if (prop === "none") return false;
      }
    }
    elem = elem.parentNode;
  }
  return false;
};

Linter.prototype.__getColor = function(style) {
  return style.getPropertyValue("color");
};

// Background color is complicated because the background color might be
// transparent, allowing an ancestor's background color to show through (note
// background color is not inherited by child nodes). If so, we need to go up
// the tree until we find a non-transparent color. Also we should ignore spans
// with CSS class "highlighted" because that's an implementation detail of the
// editor. If we get all the way to the root, return white. There are two
// versions of this function: one that uses the computed background color
// [which is normalized to "rgb(x, x, x)" or "rgba(x, x, x, x)"] and one that
// reads the backgroundColor property directly (which can be used for elements
// that are not connected to the DOM).
Linter.prototype.__getComputedBackgroundColor = getComputedBackgroundColor,

Linter.prototype.__getDirectBackgroundColor = function(root, elem) {
  while (elem !== root) {
    if (!hasCssClass(elem, "highlighted")) {
      var bg = elem.style.backgroundColor;
      if (bg && bg !== "transparent") return bg;
    }
    elem = elem.parentNode;
  }
  return "rgb(255, 255, 255)";
};

Linter.prototype.__appendTextNode = function(node, text, underline) {
  // Reuse existing text node if available.
  if (node.childNodes.length > 0) {
    var existing = logicalTextNode(node.childNodes[node.childNodes.length - 1]);
    if (existing) {
      existing.nodeValue += text;
      return;
    }
  }
  var textParent;
  if (underline) {
    textParent = document.createElement("span");
    addCssClass(textParent, "nonsyntax");
    textParent.style.textDecoration = "underline";
    node.appendChild(textParent);
  } else {
    textParent = node;
  }
  textParent.appendChild(document.createTextNode(text));
};

// Postconditions:
// * All elements are spans, and every span has at least one child text node
//   (possibly inside a nonsyntax span).
// * Every span has font size strictly less than its parent (except nonsyntax
//   spans).
// * Every nonsyntax span has exactly one child, which is a text node.
Linter.prototype.lint = function(root) {
  // Root node of linted DOM.
  var linted = document.createElement("div");
  // Span in output DOM, parent of most recently added text node.
  var outputSpan = null;
  // Traverse leaf nodes of input DOM in order. All text nodes are leaf nodes.
  for (var inputNode = leftmostLeaf(root);
       inputNode && inputNode !== root;
       inputNode = nextLeaf(root, inputNode)) {
    // TODO: Pass through <br>, etc.
    if (inputNode.nodeType !== Node.TEXT_NODE ||
        inputNode.textContent === "" ||
        inputNode.textContent === "\u200B" /* zero-width joiner */) {
      // TODO: Verify there's non-whitespace?
      continue;
    }
    var inputStyle = window.getComputedStyle(inputNode.parentNode);
    var fontSize = this.__getFontSize(inputStyle);
    var fontFamily = this.__getFontFamily(inputStyle);
    var italic = this.__getItalic(inputStyle);
    var bold = this.__getBold(inputStyle);
    var underline = this.__getIntendedUnderline(root, inputNode.parentNode);
    var color = this.__getColor(inputStyle);
    var backgroundColor =
        this.__getComputedBackgroundColor(root, inputNode.parentNode);
    // Loop until we identify the proper location in the output DOM.
    for (;;) {
      if (!outputSpan || fontSize < this.__getFontSize(outputSpan.style)) {
        // New child node.
        var parentNode = outputSpan || linted;
        outputSpan = document.createElement("span");
        outputSpan.style.fontFamily = fontFamily;
        outputSpan.style.fontSize = fontSize + "px";
        outputSpan.style.fontStyle = (italic ? "italic" : "normal");
        outputSpan.style.fontWeight = (bold ? "bold" : "normal");
        outputSpan.style.color = color;
        if (parentNode !== linted &&
            backgroundColor ===
                this.__getDirectBackgroundColor(linted, parentNode)) {
          outputSpan.style.backgroundColor = "transparent";
        } else {
          outputSpan.style.backgroundColor = backgroundColor;
        }
        this.__appendTextNode(outputSpan, inputNode.textContent, underline);
        parentNode.appendChild(outputSpan);
        break;
      } else if (fontSize === this.__getFontSize(outputSpan.style)) {
        if (fontFamily === this.__getFontFamily(outputSpan.style) &&
            italic === this.__getItalic(outputSpan.style) &&
            bold === this.__getBold(outputSpan.style) &&
            underline === isUnderlined(outputSpan) &&
            color === this.__getColor(outputSpan.style) &&
            backgroundColor ===
                this.__getDirectBackgroundColor(linted, outputSpan)) {
          // Continuation of the same node. Just add a new child text node. We
          // normalize `linted` at the end, which will concatenate adjacent
          // text nodes.
          this.__appendTextNode(outputSpan, inputNode.textContent, underline);
          break;
        } else {
          // New sibling node. Just go up a level in the output tree and redo
          // the loop so that we fall into the first case above (new child
          // node). If we happen to backtrack to the root, set outputSpan to
          // null, which will trigger the first case.
          outputSpan = outputSpan.parentNode;
          if (outputSpan === linted) outputSpan = null;
        }
      } else {
        // Go up a level in the output tree and redo the loop.
        outputSpan = outputSpan.parentNode;
        if (outputSpan === linted) outputSpan = null;
      }
    }
  }
  linted.normalize();
  // Delete old children of root, move new children from linted to root.
  root.innerHTML = "";
  while (linted.childNodes.length > 0) {
    root.appendChild(linted.childNodes[0]);
  }
};
